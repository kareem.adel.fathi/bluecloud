package com.blucloud.kareemadelfathi.blucloud;


import org.jivesoftware.smack.AccountManager;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.ChatManagerListener;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Collection;
import java.util.Iterator;

public class ChatAdmin {
    String server;
    int port;
    //ConnectionConfiguration config;

    Chat chat;
    ChatManager chatManager;
    XMPPTCPConnection con;
    String MAC;
    String MAC_compatible;
    String Name = MAC;
    String Phone = "0";
    String Image;
    String InfoDate = "0";
    String PW = "^2_QfP:+6|0*S=q";
    SqliteAdapter sqliteAdapter;
    MyNewMessageListener myNewMessageListener;
    InetAddress address;

    public ChatAdmin(String server, int port, String MAC,
                     SqliteAdapter sqliteAdapter) {

        this.sqliteAdapter = sqliteAdapter;
        this.server = server;
        this.port = port;
        this.MAC = MAC;
        this.MAC_compatible = MAC.replace(":", "_");
        address = null;

    }

    public void getMyInfo() {
        Object[] info = BluCloud.sqliteAdapter.getContactInfo(MAC);
        if (info != null) {
            Name = "" + info[0];
            Phone = "" + info[2];
            InfoDate = "" + info[3];
            Image = "" + info[4];
        }
    }

    Thread tmpInit = null;

    public void setUpConnection(String server) throws IOException, XMPPException, SmackException {
        InetAddress tmpAddress = InetAddress.getByName(server);
        String tmp = tmpAddress.getHostAddress();

        ConnectionConfiguration tmpConfig = new ConnectionConfiguration(tmp, ChatAdmin.this.port);
        tmpConfig.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
        tmpConfig.setReconnectionAllowed(false);
        tmpConfig.setCompressionEnabled(true);
        SmackConfiguration.setDefaultPacketReplyTimeout(3000);
        XMPPTCPConnection tmpCon = new XMPPTCPConnection(tmpConfig);
        tmpCon.setPacketReplyTimeout(3000);

        tmpCon.connect();
        if (con == null) {
            con = tmpCon;
            sqliteAdapter.UpdateCurrentIp(tmp);
            if (con.isConnected()) {
                tryLoggingIn();
                chatManager = ChatManager.getInstanceFor(con);
                myNewMessageListener = new MyNewMessageListener();
                chatManager.addChatListener(new ChatManagerListener() {
                    @Override
                    public void chatCreated(Chat chat, boolean createdLocally) {
                        chat.addMessageListener(myNewMessageListener);
                    }
                });
            }
        }

    }

    /*
        public void attemptConnection() throws IOException, XMPPException, SmackException {

        }
    */
    public class attemptConnectionThread extends Thread {
        String server;

        @Override
        public void run() {
            super.run();
            try {
                setUpConnection(server);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (XMPPException e) {
                e.printStackTrace();
            } catch (SmackException e) {
                e.printStackTrace();
            }
        }
    }

    public void init() {
        tmpInit = new Thread() {
            @Override
            public void run() {

                super.run();
                getMyInfo();

                attemptConnectionThread attemptConnectionTHCurrent = new attemptConnectionThread();
                attemptConnectionTHCurrent.server = sqliteAdapter.GetCurrentIp();
                attemptConnectionTHCurrent.start();
                attemptConnectionThread attemptConnectionTHNew = new attemptConnectionThread();
                attemptConnectionTHNew.server = ChatAdmin.this.server;
                attemptConnectionTHNew.start();
                /*
                try {
                    setUpConnection(sqliteAdapter.GetCurrentIp());
                    try {
                        attemptConnection();
                    } catch (SmackException e) {

                        setUpConnection(ChatAdmin.this.server);
                        try {
                            attemptConnection();
                        } catch (IOException e1) {
                            postSwitchFailed();
                            e1.printStackTrace();
                        } catch (XMPPException e1) {
                            postSwitchFailed();
                            e1.printStackTrace();
                        } catch (SmackException e1) {
                            postSwitchFailed();
                            e1.printStackTrace();
                        }
                        e.printStackTrace();

                    } catch (IOException e) {

                        setUpConnection(ChatAdmin.this.server);
                        try {
                            attemptConnection();
                        } catch (IOException e1) {
                            postSwitchFailed();
                            e1.printStackTrace();
                        } catch (XMPPException e1) {
                            postSwitchFailed();
                            e1.printStackTrace();
                        } catch (SmackException e1) {
                            postSwitchFailed();
                            e1.printStackTrace();
                        }
                        e.printStackTrace();

                    } catch (XMPPException e) {

                        setUpConnection(ChatAdmin.this.server);
                        try {
                            attemptConnection();
                        } catch (IOException e1) {
                            postSwitchFailed();
                            e1.printStackTrace();
                        } catch (XMPPException e1) {
                            postSwitchFailed();
                            e1.printStackTrace();
                        } catch (SmackException e1) {
                            postSwitchFailed();
                            e1.printStackTrace();
                        }
                        e.printStackTrace();

                    }

                } catch (UnknownHostException e) {
                    e.printStackTrace();
                    postSwitchFailed();
                    return;
                } catch (XMPPException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (SmackException e) {
                    e.printStackTrace();
                }
*/
            }
        };
        tmpInit.start();
    }


    Thread tmpDisconnect = null;

    public void Disconnect() {
        tmpDisconnect = new Thread() {
            @Override
            public void run() {
                super.run();
                try {
                    if (tmpInit != null) {
                        if (con != null) {
                            con.disconnect();
                        }
                        tmpInit.interrupt();
                        tmpInit = null;
                        con = null;
                    }
                    postSwitchDisconnected();
                } catch (SmackException.NotConnectedException e) {
                    e.printStackTrace();
                    postSwitchFailed();
                }
            }
        };
        tmpDisconnect.start();
    }

    public void postSwitchConnected() {
        BluCloud.ISwitch.post(new Runnable() {
            public void run() {
                BluCloud.ISwitch.setText("Internet Connected");
            }
        });
    }

    public void postSwitchDisconnected() {
        BluCloud.ISwitch.post(new Runnable() {
            public void run() {
                BluCloud.ISwitch.setText("Internet Disconnected");
            }
        });
    }

    public void postSwitchFailed() {
        BluCloud.ISwitch.post(new Runnable() {
            public void run() {
                BluCloud.ISwitch.setText("Internet Failed");
            }
        });
    }

    public void tryLoggingIn() {

        try {
            con.login(MAC_compatible, PW);
            postSwitchConnected();
            BluCloud.internetConnection = true;

        } catch (XMPPException e) {
            e.printStackTrace();
            try {
                CreateAccount(MAC_compatible);
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
                try {
                    con.disconnect();
                } catch (SmackException.NotConnectedException e1) {
                    e1.printStackTrace();
                }

                try {
                    con.connect();
                    con.login(MAC_compatible, PW);
                    postSwitchConnected();
                    BluCloud.internetConnection = true;
                } catch (SmackException x) {
                    postSwitchConnected();
                    x.printStackTrace();
                } catch (IOException x) {
                    postSwitchFailed();
                    x.printStackTrace();
                } catch (XMPPException x) {
                    postSwitchFailed();
                    x.printStackTrace();
                }

            } catch (SmackException.NoResponseException e1) {
                postSwitchFailed();
                e1.printStackTrace();
            } catch (XMPPException.XMPPErrorException e1) {
                postSwitchFailed();
                e1.printStackTrace();
            } catch (SmackException.NotConnectedException e1) {
                postSwitchFailed();
                e1.printStackTrace();
            }
        } catch (SmackException e) {
            postSwitchConnected();
            e.printStackTrace();
        } catch (IOException e) {
            postSwitchFailed();
            e.printStackTrace();
        }


    }

    String tmp_MAC;

    public void CreateAccount(String MAC) throws SmackException.NoResponseException,
            XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        tmp_MAC = MAC;
        Thread tmp = new Thread() {
            @Override
            public void run() {
                super.run();
                AccountManager accountManager = AccountManager.getInstance(con);
                try {
                    accountManager.createAccount(tmp_MAC, PW);
                } catch (SmackException.NoResponseException e) {
                    e.printStackTrace();
                } catch (XMPPException.XMPPErrorException e) {
                    e.printStackTrace();
                } catch (SmackException.NotConnectedException e) {
                    e.printStackTrace();
                }
            }
        };
        tmp.start();
    }

    public void sendMessageNow(String Text, String ToMAC, String date)
            throws SmackException.NotConnectedException, XMPPException {
        if (chatManager != null) {
            String toAddress = ToMAC.replace(":", "_") + "@kareem/Smack";
            if (chat == null
                    || !chat.getParticipant().equals(toAddress)) {
                chat = chatManager.createChat(toAddress, null);
            }
            Message message = new Message();
            // message_remote.setFrom(Name);
            //message_remote.setTo(Phone + "," + InfoDate + "," + Image);
            //message_remote.setSubject("" + System.currentTimeMillis());
            message.addSubject("sendDate", "" + date);
            message.addSubject("fromName", Name);
            message.addSubject("Phone", Phone);
            message.addSubject("InfoDate", InfoDate);
            message.addSubject("Image", Image);
            message.setBody(Text);

            chat.sendMessage(message);
        } else {
            //not connected
        }
    }

    class MyNewMessageListener implements MessageListener {

        @Override
        public void processMessage(Chat arg0, Message arg1) {

            Collection<Message.Subject> subjects = arg1.getSubjects();
            Iterator<Message.Subject> iterator = subjects.iterator();

            String FromMAC = arg0.getParticipant().split("@")[0].replace("_",
                    ":").toUpperCase();

            String sendDate = "";
            String fromName = "";
            String Phone = "";
            String InfoDate = "";
            String Image = "";

            while (iterator.hasNext()) {
                Message.Subject subject = iterator.next();
                String property = subject.getLanguage();
                String value = subject.getSubject();
                if (property.equals("sendDate")) {
                    sendDate = value;
                } else if (property.equals("fromName")) {
                    fromName = value;
                } else if (property.equals("Phone")) {
                    Phone = value;
                } else if (property.equals("InfoDate")) {
                    InfoDate = value;
                } else if (property.equals("Image")) {
                    Image = value;
                }
            }


            AllData allData = new AllData();
            allData.From_To_Message_Date = new Object[4][1];
            allData.From_To_Message_Date[0][0] = FromMAC;
            allData.From_To_Message_Date[1][0] = MAC;
            allData.From_To_Message_Date[2][0] = arg1.getBody();
            allData.From_To_Message_Date[3][0] = Long.parseLong(sendDate);

            allData.Name_Mac_Phone_Date_pic = new Object[5][1];
            allData.Name_Mac_Phone_Date_pic[0][0] = fromName;
            allData.Name_Mac_Phone_Date_pic[1][0] = FromMAC;
            allData.Name_Mac_Phone_Date_pic[2][0] = Phone;
            allData.Name_Mac_Phone_Date_pic[3][0] = Long.parseLong(InfoDate);
            allData.Name_Mac_Phone_Date_pic[4][0] = Image;

            sqliteAdapter.insertTableTrans(allData);


        }

    }

}
