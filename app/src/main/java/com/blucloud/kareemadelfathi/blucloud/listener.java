package com.blucloud.kareemadelfathi.blucloud;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

/**
 * This class does all the work for setting up and managing Bluetooth
 * connections with other devices. It has a thread that listens for incoming
 * connections, a thread for connecting with a device, and a thread for
 * performing data transmissions when connected.
 */
public class listener {
    // Debugging
    private static final String TAG = "BluetoothChatService";
    private static final boolean D = true;

    // Name for the SDP record when creating server socket
    private static final String NAME = "BluCloud";
    // Unique UUID for this application
    private static final UUID[] MY_UUID = new UUID[]{
            UUID.fromString("0bacf314-4da3-11e4-9e35-164230d1df67"),
            UUID.fromString("0bacf756-4da3-11e4-9e35-164230d1df67"),
            UUID.fromString("0bacf8b4-4da3-11e4-9e35-164230d1df67"),
            UUID.fromString("0bacf9ea-4da3-11e4-9e35-164230d1df67"),
            UUID.fromString("0bacfb20-4da3-11e4-9e35-164230d1df67"),
            UUID.fromString("0bacfc4c-4da3-11e4-9e35-164230d1df67"),
            UUID.fromString("0bacfd6e-4da3-11e4-9e35-164230d1df67")};

    // Member fields
    private final BluetoothAdapter mAdapter;
    private final Handler mHandler;
    private AcceptThread currentAcceptThread;
    // public ArrayList<ConnectedThread> mConnectedThreads;
    // private ConnectThread mConnectThread;
    // private ConnectedThread mConnectedThread;
    // public ArrayList<ConnectThread> mConnectThreads;
    // public ArrayList<ConnectedThread> mConnectedThreads = new
    // ArrayList<ConnectedThread>();
    ConnectedThread currentConnectedThread = null;
    private int mState;
    // Constants that indicate the current connection state
    public static final int STATE_NONE = 0; // we're doing nothing
    public static final int STATE_LISTEN = 1; // now listening for incoming
    // connections
    public static final int STATE_CONNECTING = 2; // now initiating an outgoing
    // connection
    public static final int STATE_CONNECTED_CLIENT = 3; // now connected to a
    // remote device
    public static final int STATE_CONNECTED_SERVER = 4;

    /**
     * Constructor. Prepares a new BluCloud session.
     *
     * @param context The UI Activity Context
     * @param handler A Handler to send messages back to the UI Activity
     */

    public listener(Context context, Handler handler, BluCloud parent) {
        mAdapter = BluetoothAdapter.getDefaultAdapter();
        // mAdapter.
        mState = STATE_NONE;
        mHandler = handler;
    }

    /**
     * Set the current state of the chat connection
     *
     * @param state An integer defining the current connection state
     */
    private void setState(int state) {
        if (D)
            Log.d(TAG, "setState() " + mState + " -> " + state);
        mState = state;

        // Give the new state to the Handler so the UI Activity can update
        mHandler.obtainMessage(BluCloud.MESSAGE_STATE_CHANGE, state, -1)
                .sendToTarget();
    }

    /**
     * Return the current connection state.
     */
    public int getState() {
        return mState;
    }

    public synchronized void clearCurrentConnections() {
        // int length = mConnectedThreads.size();
        // for (int i = 0; i < length; i++) {
        // ConnectedThread tmp = mConnectedThreads.get(i);
        // tmp.cancel();
        // tmp = null;
        // }
        // mConnectedThreads.clear();
        if (currentConnectedThread != null) {
            currentConnectedThread.cancel();
            currentConnectedThread = null;
        }
        if (currentAcceptThread != null) {
            currentAcceptThread.cancel();
            currentAcceptThread = null;
        }
        serverSideClient = null;
    }

    /**
     * Start the chat service. Specifically start AcceptThread to begin a
     * session in listening (server) mode. Called by the Activity onResume()
     */
    public void start() {
        if (D)
            Log.d(TAG, "start");

        // Cancel any thread attempting to make a connection
        // if (mConnectThread != null) {mConnectThread.cancel(); mConnectThread
        // = null;}

        // Cancel any thread currently running a connection
        // if (mConnectedThread != null) {mConnectedThread.cancel();
        // mConnectedThread = null;}

        clearCurrentConnections();

        // Start the thread to listen on a BluetoothServerSocket
        if (currentAcceptThread == null) {
            currentAcceptThread = new AcceptThread(0);
            currentAcceptThread.start();
        }
        setState(STATE_LISTEN);
    }

    /**
     * Start the ConnectThread to initiate a connection to a remote device.
     *
     * @param device
     *            The BluetoothDevice to connect
     */
    /*
     * public synchronized void connect(BluetoothDevice device) { if (D)
	 * Log.d(TAG, "connect to: " + device);
	 * 
	 * String names[] = {device.getName(),device.getAddress()};
	 * setRemoteBluetoothName(names);
	 * 
	 * // Cancel any thread attempting to make a connection if (mState ==
	 * STATE_CONNECTING) { if (mConnectThread != null) {mConnectThread.cancel();
	 * mConnectThread = null;} }
	 * 
	 * // Cancel any thread currently running a connection if (mConnectedThread
	 * != null) {mConnectedThread.cancel(); mConnectedThread = null;}
	 * 
	 * // Start the thread to connect with the given device mConnectThread = new
	 * ConnectThread(device); mConnectThread.start();
	 * setState(STATE_CONNECTING); }
	 */

	/*
	 * class looperThread extends Thread {
	 * 
	 * @Override public void run() { handler.postDelayed(new Runnable() {
	 * 
	 * @Override public void run() { SendToConnected(0,
	 * BluCloud.DBAllData); (new looperThread()).start(); } }, 3000); } }
	 * 
	 * public void SendToConnected(int i, byte[] data) { try { if
	 * (mConnectedThreads != null && mConnectedThreads.size() > 0) { if (i >
	 * mConnectedThreads.size()) { // i = 0; // SendToConnected(i, data);
	 * return; } if (mConnectedThreads.get(i) != null) {
	 * 
	 * // synchronized (this) {
	 * 
	 * mConnectedThreads.get(i).write(data);
	 * 
	 * // } i++; SendToConnected(i, data); } } } catch (Exception e) { // TODO
	 * Auto-generated catch block e.printStackTrace(); } }
	 */
    /**
     * Start the ConnectedThread to begin managing a Bluetooth connection
     *
     * @param socket
     * The BluetoothSocket on which the connection was made
     * @param device
     * The BluetoothDevice that has been connected
     */
    public static BluetoothDevice serverSideClient = null;

    public synchronized void connected(BluetoothSocket socket,
                                       BluetoothDevice device) {
        if (D)
            Log.d(TAG, "connected");

        currentConnectedThread = new ConnectedThread(socket);
        currentConnectedThread.start();
        serverSideClient = device;
        Message msg = mHandler.obtainMessage(BluCloud.MESSAGE_DEVICE_NAME);
        Bundle bundle = new Bundle();
        bundle.putString(BluCloud.DEVICE_NAME, device.getName());
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        // sendMessage("test-server");
        setState(STATE_CONNECTED_SERVER);
    }

    /**
     * Stop all threads
     */
    public void stop() {
        if (D)
            Log.d(TAG, "stop");
        // if (mConnectThread != null) {mConnectThread.cancel(); mConnectThread
        // = null;}

        clearCurrentConnections();

        if (currentAcceptThread != null) {
            currentAcceptThread.cancel();
            currentAcceptThread = null;
        }
        setState(STATE_NONE);
    }

	/*
	 * public void sendMessage(String message_remote) { byte[] send =
	 * message_remote.getBytes(); write(send); }
	 */
    /**
     * Write to the ConnectedThread in an unsynchronized manner
     *
     * @param out
     *            The bytes to write
     * @see com.blucloud.kareemadelfathi.blucloud.listener.ConnectedThread#write(byte[])
     */
	/*
	 * public synchronized void write(byte[] out) { // Create temporary object
	 * ConnectedThread r; // Synchronize a copy of the ConnectedThread int
	 * length = mConnectedThreads.size(); for (int i = 0; i < length; i++) {
	 * synchronized (this) { // if (mState != STATE_CONNECTED_CLIENT) // return;
	 * r = mConnectedThreads.get(i); } // Perform the write unsynchronized
	 * r.write(out); }
	 * 
	 * }
	 */
    /**
     * Indicate that the connection attempt failed and notify the UI Activity.
     */
	/*
	private void connectionFailed() {
		setState(STATE_LISTEN);

		// Send a failure message_remote back to the Activity
		Message msg = mHandler.obtainMessage(BluCloud.MESSAGE_TOAST);
		Bundle bundle = new Bundle();
		bundle.putString(BluCloud.TOAST, "Unable to connect device");
		msg.setData(bundle);
		mHandler.sendMessage(msg);
		// stop();
		// parent.freeMemory(this);
	}
*/
    /**
     * Indicate that the connection was lost and notify the UI Activity.
     */
	/*
	private void connectionLost() {

		setState(STATE_LISTEN);
		// Send a failure message_remote back to the Activity
		Message msg = mHandler.obtainMessage(BluCloud.CONNECTION_LOST);
		Bundle bundle = new Bundle();
		bundle.putString(BluCloud.TOAST, "Device connection was lost");
		msg.setData(bundle);
		mHandler.sendMessage(msg);
		// stop();
		// parent.freeMemory(this);
	}
*/

    /**
     * This thread runs while listening for incoming connections. It behaves
     * like a server-side client. It runs until a connection is accepted (or
     * until cancelled).
     */

    private class AcceptThread extends Thread {
        // The local server socket
        private final BluetoothServerSocket mmServerSocket;

        //int x;

        public AcceptThread(int x) {
            BluetoothServerSocket tmp = null;
            //this.x = x;
            // Create a new listening server socket
            try {

                tmp = mAdapter.listenUsingInsecureRfcommWithServiceRecord(NAME,
                        MY_UUID[x]);
            } catch (IOException e) {
                Log.e(TAG, "listen() failed", e);
            }
            mmServerSocket = tmp;
        }

        public void run() {
            if (D)
                Log.d(TAG, "BEGIN mAcceptThread" + this);
            setName("AcceptThread");
            BluetoothSocket socket = null;
            try {
                if (mmServerSocket != null)
                    socket = mmServerSocket.accept(20000);

            } catch (IOException e) {
                Log.e(TAG, "accept() failed", e);

            }

            // If a connection was accepted
            if (socket != null) {

                connected(socket, socket.getRemoteDevice());
                if (mmServerSocket != null) {
                    try {
                        mmServerSocket.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                // if (x < 5) {
                // if (mmServerSocket != null) {
                // try {
                // mmServerSocket.close();
                // } catch (IOException e) {
                // TODO Auto-generated catch block
                // e.printStackTrace();
                // }
                // }
                // currentAcceptThread = new AcceptThread(++x);
                // currentAcceptThread.start();
                // }
                if (D)
                    Log.i(TAG, "END mAcceptThread");

            }
			/*
			 * else { try { Thread.sleep(2000); } catch (InterruptedException
			 * e1) { // TODO Auto-generated catch block e1.printStackTrace(); }
			 * try { if (mmServerSocket != null) mmServerSocket.close(); } catch
			 * (IOException e) { // TODO Auto-generated catch block
			 * e.printStackTrace(); } currentAcceptThread = new AcceptThread(x);
			 * currentAcceptThread.start(); }
			 */
        }

        public void cancel() {
            if (D)
                Log.d(TAG, "cancel " + this);
            try {
                mmServerSocket.close();
                // this.interrupt();
            } catch (IOException e) {
                Log.e(TAG, "close() of server failed", e);
            }
        }
    }

    /**
     * This thread runs while attempting to make an outgoing connection with a
     * device. It runs straight through; the connection either succeeds or
     * fails.
     */
	/*
	 * private class ConnectThread extends Thread { private final
	 * BluetoothSocket mmSocket; private final BluetoothDevice mmDevice;
	 * 
	 * public ConnectThread(BluetoothDevice device) { mmDevice = device;
	 * BluetoothSocket tmp = null;
	 * 
	 * // Get a BluetoothSocket for a connection with the // given
	 * BluetoothDevice try { tmp =
	 * device.createInsecureRfcommSocketToServiceRecord (MY_UUID); } catch
	 * (IOException e) { Log.e(TAG, "create() failed", e); } mmSocket = tmp; }
	 * 
	 * public void run() { Log.i(TAG, "BEGIN mConnectThread");
	 * setName("ConnectThread");
	 * 
	 * // Always cancel discovery because it will slow down a connection
	 * mAdapter.cancelDiscovery();
	 * 
	 * // Make a connection to the BluetoothSocket try { // This is a blocking
	 * call and will only return on a // successful connection or an exception
	 * mmSocket.connect(); } catch (IOException e) { connectionFailed(); //
	 * Close the socket try { mmSocket.close(); } catch (IOException e2) {
	 * Log.e(TAG, "unable to close() socket during connection failure", e2); }
	 * // Start the service over to restart listening mode this.start(); return;
	 * }
	 * 
	 * // Reset the ConnectThread because we're done synchronized (this) {
	 * mConnectThread = null; }
	 * 
	 * // Start the connected thread connected(mmSocket, mmDevice); }
	 * 
	 * public void cancel() { try { mmSocket.close(); } catch (IOException e) {
	 * Log.e(TAG, "close() of connect socket failed", e); } } }
	 */

    /**
     * This thread runs during a connection with a remote device. It handles all
     * incoming and outgoing transmissions.
     */
    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        //Timer timerReciever;
        //TimerTask timerRecieverDescription;
        //Timer timerSender;
        //TimerTask timerSenderDescription;

        public ConnectedThread(BluetoothSocket socket) {
            Log.d(TAG, "create ConnectedThread");
            mmSocket = socket;
            InputStream tmpIn = null;
            //OutputStream tmpOut = null;

            // Get the BluetoothSocket input and output streams
            try {
                tmpIn = socket.getInputStream();
                //tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.e(TAG, "temp sockets not created", e);
            }

            mmInStream = tmpIn;
            /*
            timerRecieverDescription = new TimerTask() {
                @Override
                public void run() {
                    try {
                        if (mmInStream.available() > 0) {
                            mmInStream.read(tmpDataSize);
                            String tmp = "";
                            for (int i = 0; i < tmpDataSize.length; i++) {
                                if (tmpDataSize[i] == 0) {
                                    tmp += "0";
                                } else {
                                    tmp += "1";
                                }
                            }
                            tmpDataSize = new byte[32];
                            int dataLength = Integer.parseInt(tmp, 2);
                            for (int i = 0; i < dataLength; i++) {
                                baos.write(mmInStream.read());
                            }
                            if (baos.size() != 0) {
                                mHandler.obtainMessage(
                                        BluCloud.MESSAGE_READ,
                                        baos.size(), -1, baos.toByteArray())
                                        .sendToTarget();
                                try {
                                    baos.close();
                                } catch (IOException e1) {
                                    // TODO Auto-generated catch block
                                    e1.printStackTrace();
                                }
                                baos = new ByteArrayOutputStream();

                            }
                        }
                    } catch (IOException e) {
                        Log.e(TAG, "disconnected", e);

                    }
                }
            };
            timerReciever = new Timer("timerReciever");

            timerSenderDescription = new TimerTask() {
                @Override
                public void run() {
                    if (this != null && mmSocket != null
                            && mmSocket.isConnected()) {
                        write(BluCloud.DBAllData);
                    }
                }
            };
            timerSender = new Timer("timerSender");

            // handler.getLooper();
            // (new senderThread()).start();
            */
        }

        byte[] tmpDataSize = new byte[32];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        // recieverThread rt;

        public void run() {
            Log.i(TAG, "BEGIN mConnectedThread");
            // write(BluCloud.DBAllData);
            //timerReciever
            //        .scheduleAtFixedRate(timerRecieverDescription, 0, 1500);
            //timerSender.scheduleAtFixedRate(timerSenderDescription, 0, 2000);

            while (mmSocket != null && mmSocket.isConnected()) {
                try {
                    if (mmInStream.available() > 0) {
                        mmInStream.read(tmpDataSize);
                        String tmp = "";
                        for (int i = 0; i < tmpDataSize.length; i++) {
                            if (tmpDataSize[i] == 0) {
                                tmp += "0";
                            } else {
                                tmp += "1";
                            }
                        }
                        tmpDataSize = new byte[32];
                        int dataLength = Integer.parseInt(tmp, 2);
                        for (int i = 0; i < dataLength; i++) {
                            baos.write(mmInStream.read());
                        }
                        if (baos.size() != 0) {
                            mHandler.obtainMessage(
                                    BluCloud.MESSAGE_READ,
                                    baos.size(), -1, baos.toByteArray())
                                    .sendToTarget();
                            try {
                                baos.close();
                            } catch (IOException e1) {
                                // TODO Auto-generated catch block
                                e1.printStackTrace();
                            }
                            baos = new ByteArrayOutputStream();

                        }
                    }
                } catch (IOException e) {
                    Log.e(TAG, "disconnected", e);

                }

                if (this != null && mmSocket != null
                        && mmSocket.isConnected()) {
                    write(BluCloud.DBAllData);
                }

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }

            // rt = new recieverThread();
            // rt.start();
        }

        public byte[] includeSizeToBuffer(byte[] buffer) throws IOException {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            String sizeInBinaryStringIncomplete = Integer
                    .toBinaryString(buffer.length);
            String sizeInBinaryString = "";
            for (int i = sizeInBinaryStringIncomplete.length(); i <= 31; i++) {
                sizeInBinaryString += "0";
            }
            sizeInBinaryString += sizeInBinaryStringIncomplete;
            byte[] tmp = new byte[sizeInBinaryString.length()];
            for (int i = 0; i <= 31; i++) {
                if (sizeInBinaryString.charAt(i) == '0') {
                    tmp[i] = 0;
                } else {
                    tmp[i] = 1;
                }
            }
            baos.write(tmp);
            baos.write(buffer);
            return baos.toByteArray();
        }

        /**
         * Write to the connected OutStream.
         *
         * @param buffer
         * The bytes to write
         */
        byte[] last_sent;

        public synchronized void write(byte[] buffer) {
            try {
                if (last_sent != buffer) {
                    mmSocket.getOutputStream().write(
                            includeSizeToBuffer(buffer));
                    last_sent = buffer;
                    BluCloud.tmp_SENT++;
                }
                // Share the sent message_remote back to the UI Activity
                // mHandler.obtainMessage(BluCloud.MESSAGE_WRITE, -1, -1,
                // buffer).sendToTarget();
            } catch (IOException e) {
                Log.e(TAG, "Exception during write , socket connection is : "
                        + mmSocket.isConnected(), e);

            }
        }

        public void cancel() {
            try {
                //timerReciever.cancel();
                //timerSender.cancel();
                mmSocket.getOutputStream().close();
                mmSocket.getInputStream().close();
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "close() of connect socket failed", e);
            }
        }
    }
}
