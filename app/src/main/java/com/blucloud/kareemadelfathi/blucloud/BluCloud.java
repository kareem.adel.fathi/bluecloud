package com.blucloud.kareemadelfathi.blucloud;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;

import java.io.IOException;
import java.util.ArrayList;

/**
 * This is the main Activity that displays the current chat session.
 */
public class BluCloud extends Activity {
    // Debugging
    private static final String TAG = "BluCloud";
    private static final boolean D = true;
    public static SqliteAdapter sqliteAdapter;
    public static boolean DBChanged = true;
    public static byte[] DBAllData;
    public static int tmp_SENT = 0;
    public static int tmp_RECIEVED = 0;

    // Message types sent from the BluetoothChatService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    public static final int CONNECTION_LOST = 6;

    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    //public static final String TOAST = "toast";

    private static final int REQUEST_ENABLE_BT = 2;

    private ListView mConversationView;
    private ListView mContactsView;
    private ListView mListPreview;
    private EditText mOutEditText;
    private Button mSendButton;
    private ImageView profileImage;
    private EditText MyName;
    private EditText PhoneNum;
    private int myChosenImage;

    ArrayList<Object[]> Contacts;
    //ContactsInfoAdapter contactsInfoAdapter;
    //Calendar cal;

    public BluetoothAdapter mBluetoothAdapter = null;
    public BluCloud mainBluCloud = this;

    Client mChatServicesClient;
    listener mChatServicesListener;
    public ArrayList<BluetoothDevice> mNewDevices = new ArrayList<BluetoothDevice>();
    ActionBarDrawerToggle DrawerListener;
    DrawerLayout Dl;
    FrameLayout frame;
    public static ChatAdmin admin;

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        DrawerListener.syncState();
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // TODO Auto-generated method stub
        super.onConfigurationChanged(newConfig);
        DrawerListener.onConfigurationChanged(newConfig);
    }

    TextView status;

    public synchronized void updateStatus(int sent, int received) {
        status.setText("\nsummary \nSent : " + sent + "\nReceived : "
                + received);
    }

    public void createNotification(String contentTitle, String contentText, String Ticker, String Description) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this).setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)).setSmallIcon(R.drawable.ic_launcher).setContentTitle(contentTitle).setContentText(contentText).setTicker(Ticker).setContentInfo(Description).setLights(0xFFFF0000, 1000, 1000).setAutoCancel(true);
        Intent resultIntent = new Intent(this, BluCloud.class);

        resultIntent.setAction("android.intent.action.MAIN");
        resultIntent.addCategory("android.intent.category.LAUNCHER");

        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, builder.build());
    }

    static boolean internetConnection = false;
    static Switch ISwitch;
    static Switch BSwitch;

    public void BluetoothSwitchClicked(View v) {
        BSwitch = (Switch) v;
        if (BSwitch.isChecked()) {
            BSwitch.setText("Bluetooth Connecting ...");
            startBTAdapter();
        } else {
            BSwitch.setText("Bluetooth Disconnecting ...");
            stopBTAdapter();
        }

    }

    public void InternetSwitchClicked(View v) {
        ISwitch = (Switch) v;
        if (admin != null) {
            if (ISwitch.isChecked()) {
                ISwitch.setText("Internet Connecting ...");
                if (admin.con == null) {

                    admin.init();
                } else {
                    if (!admin.con.isConnected()) {
                        admin.init();
                    } else {
                        ISwitch.setText("Internet Connected");
                    }
                }
            } else {
                ISwitch.setText("Internet Disconnecting ...");
                admin.Disconnect();
                internetConnection = false;
            }
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (D)
            Log.e(TAG, "+++ ON CREATE +++");
        //ISwitch = (Switch) findViewById(R.id.ISwitch);
        //BSwitch = (Switch) findViewById(R.id.BSwitch);

        // cal = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
        // cal.getTime().getTime();
        sqliteAdapter = new SqliteAdapter(this, this);
        // Set up the window layout
        setContentView(R.layout.main);
        ISwitch = (Switch) findViewById(R.id.ISwitch);
        BSwitch = (Switch) findViewById(R.id.BSwitch);
        // ----------------------------------------------------------------------
        frame = (FrameLayout) findViewById(R.id.content_frame);
        DrawerLayout Dl = (DrawerLayout) findViewById(R.id.drawer_layout);
        this.Dl = Dl;
        DrawerListener = new ActionBarDrawerToggle(this, Dl,
                R.drawable.ic_drawer, R.string.OpenDrawer, R.string.CloseDrawer);
        Dl.setDrawerListener(DrawerListener);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        // ----------------------------------------------------------------------
        ListView options = (ListView) findViewById(R.id.leftDrawerListView);
        options.setAdapter(new OptionsItemsAdapter(getApplicationContext()));
        options.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
                                    long arg3) {

                switch (pos) {
                    case 0: {
                        // Recent
                        setupRecentConversations();
                        break;
                    }

                    case 1: {
                        // Contacts
                        setupAllContacts();
                        break;
                    }

                    case 2: {
                        // Settings
                        setupSettings();
                        break;
                    }

                    case 3: {
                        // About
                        PutInMainContentFrame(R.layout.about);
                        break;
                    }

                }
                (BluCloud.this).Dl.closeDrawers();
            }
        });
        // ----------------------------------------------------------------------
        // Register for broadcasts when a device is discovered
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        this.registerReceiver(mReceiver, filter);

        // Register for broadcasts when discovery has finished
        filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        this.registerReceiver(mReceiver, filter);

        startBTAdapter();

        status = (TextView) findViewById(R.id.status);
        setupRecentConversations();
        //////////////////////////////////////////////////////////
        if (admin == null) {
            admin = new ChatAdmin("blu.ddns.net", 8080,
                    //blu.dnsdynamic.com "blu.ddns.net"
                    mBluetoothAdapter.getAddress(), sqliteAdapter);
        }

        isStopped = false;
    }

    boolean noAdapter = false;

    public void stopBTAdapter() {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available",
                    Toast.LENGTH_LONG).show();
            if (BSwitch != null) BSwitch.setText("Bluetooth is not available");
            noAdapter = true;
            //finish();
            return;
        }
        if (mBluetoothAdapter.isEnabled()) {
            //handler.
            if (handler != null && RunnableFunctionHandler != null)
                handler.removeCallbacks(RunnableFunctionHandler);
            if (handler1 != null && RunnableFunctionHandler1 != null)
                handler1.removeCallbacks(RunnableFunctionHandler1);
            if (currentBluThread != null) currentBluThread.interrupt();
            currentBluThread = null;
            mBluetoothAdapter.cancelDiscovery();
            mBluetoothAdapter.disable();
        } else {
            if (handler != null && RunnableFunctionHandler != null)
                handler.removeCallbacks(RunnableFunctionHandler);
            if (handler1 != null && RunnableFunctionHandler1 != null)
                handler1.removeCallbacks(RunnableFunctionHandler1);
            if (currentBluThread != null) currentBluThread.interrupt();
            currentBluThread = null;
        }
        if (BSwitch != null)
            BSwitch.setText("Bluetooth Disconnected");
    }

    public void startBTAdapter() {
        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available",
                    Toast.LENGTH_LONG).show();
            noAdapter = true;
            //finish();
            if (BSwitch != null) BSwitch.setText("Bluetooth is not available");
            return;
        }

        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            // Otherwise, setup the chat session

        } else {
            // PutInMainContentFrame(R.layout.chat);
            if (BSwitch != null) BSwitch.setText("Bluetooth Connected");
            ensureDiscoverable();
            insertMyInfo(
                    "User" + mBluetoothAdapter.getAddress().replace(":", ""),
                    "0", 0, "" + 0);
            // setupChat("example");
            startProcessCycle();
            sqliteAdapter.getAllDataTrans();
            // setupChat();
        }


    }

    /*
        @Override
        public void onStart() {
            super.onStart();
            if (D)
                Log.e(TAG, "++ ON START ++");

            // If BT is not on, request that it be enabled.
            // setupChat() will then be called during onActivityResult
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableIntent = new Intent(
                        BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
                // Otherwise, setup the chat session

            } else {
                // PutInMainContentFrame(R.layout.chat);
                ensureDiscoverable();
                insertMyInfo(
                        "User" + mBluetoothAdapter.getAddress().replace(":", ""),
                        "0", 0, "" + 0);
                // setupChat("example");
                startProcessCycle();
                sqliteAdapter.getAllDataTrans();
                // setupChat();
            }

            /*
            //////////////////////////////////////////

            try {
                admin.sendMessageNow("test message_remote", mBluetoothAdapter.getAddress());
            } catch (SmackException.NotConnectedException e) {
                e.printStackTrace();
            } catch (XMPPException e) {
                e.printStackTrace();
            }
    ////////////////////////////////////////

        }
    */
    public void UpdateProfile(View v) {
        insertMyInfo(MyName.getText().toString(),
                PhoneNum.getText().toString(), System.currentTimeMillis(), ""
                        + myChosenImage);

        Toast.makeText(this, "Your profile has been updated",
                Toast.LENGTH_SHORT).show();
    }

    public void insertMyInfo(String Name, String Phone, long Date,
                             String myChosenImage) {
        String Mac = mBluetoothAdapter.getAddress();
        if (sqliteAdapter.getContactInfo(Mac) == null) {
            sqliteAdapter.insertContactDetails(Name, Mac, Phone, 0,
                    myChosenImage);
        } else {
            if (Date != 0) {
                sqliteAdapter.UpdateContactsDetails(Name, Mac, Phone, Date,
                        myChosenImage);
                sqliteAdapter.getAllDataTrans();
            }

        }
    }

    @Override
    public synchronized void onResume() {
        super.onResume();
        if (D)
            Log.e(TAG, "+ ON RESUME +");
        if ((mChatServicesClient != null) && (mChatServicesClient != null)) {
            mChatServicesClient.stop();
            mChatServicesListener.stop();
        }
        mChatServicesClient = new Client(this, mHandler, mainBluCloud);
        mChatServicesListener = new listener(mainBluCloud, mHandler,
                mainBluCloud);
        // currenframe = 1;

    }

    public View PutInMainContentFrame(int View) {

        LayoutInflater inflator = (LayoutInflater) getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(View, null, false);

        frame.removeAllViews();
        frame.addView(v);
        return v;
    }

    public int getImageByNum(int index) {
        switch (index) {
            case 0: {
                return R.drawable.a11;
            }
            case 1: {
                return R.drawable.a12;
            }
            case 2: {
                return R.drawable.a21;
            }
            case 3: {
                return R.drawable.a22;
            }
            case 4: {
                return R.drawable.a31;
            }
            case 5: {
                return R.drawable.a32;
            }
            case 6: {
                return R.drawable.a41;
            }
            case 7: {
                return R.drawable.a42;
            }
            case 8: {
                return R.drawable.a51;
            }
            case 9: {
                return R.drawable.a52;
            }

        }

        return 0;
    }

    private void setupSettings() {
        View v = PutInMainContentFrame(R.layout.my_info);
        MyName = (EditText) v.findViewById(R.id.UserName);
        PhoneNum = (EditText) v.findViewById(R.id.PhoneNumber);
        profileImage = (ImageView) v.findViewById(R.id.ProfileImage);
        mListPreview = (ListView) findViewById(R.id.PreviewList);
        mListPreview.setAdapter(new MyInfoAdapter(this));
        Object[] info = BluCloud.sqliteAdapter
                .getContactInfo(mBluetoothAdapter.getAddress());
        String Name = (String) info[0];
        String Phone = (String) info[2];
        String Image = (String) info[4];
        MyName.setText(Name);
        if ((Phone == null) || (Phone.equals("0"))) {
            PhoneNum.setText("");
        } else {
            PhoneNum.setText(Phone);
        }
        if (Image != null) {
            profileImage
                    .setImageResource(getImageByNum(Integer.parseInt(Image)));
        }

        mListPreview.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                // TODO Auto-generated method stub
                myChosenImage = arg2;
                profileImage.setImageResource(getImageByNum(arg2));
            }
        });
    }

    private void setupAllContacts() {
        currentOtherUser = "";
        // setContentView(R.layout.active_conversations);
        View v = PutInMainContentFrame(R.layout.all_contacts);
        ArrayList<Object[]> Source = sqliteAdapter.getContacts();
        Contacts = Source;
        mContactsView = (ListView) v.findViewById(R.id.all_Contacts_list);
        // mContactsView = activeConversationsFragment.Contacts_List;
        if (Contacts != null) {
            mContactsView.setAdapter(new ContactsInfoAdapter(this, Contacts
                    .size()));
            mContactsView.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1,
                                        int arg2, long arg3) {
                    // TODO Auto-generated method stub
                    String remoteMac = ((String) (Contacts.get(arg2)[1]));
                    setupChat(remoteMac);
                }
            });
        } else {
            String[] tmp = {"There are no Contacts"};
            mContactsView.setAdapter(new ArrayAdapter<String>(this,
                    R.layout.basic_list_item_text, R.id.EmptyConv, tmp));
        }
        currenframe = 2;
    }

    public void setupRecentConversations() {
        // setContentView(R.layout.active_conversations);
        currentOtherUser = "";
        View v = PutInMainContentFrame(R.layout.active_conversations);
        ArrayList<Object[]> Source = sqliteAdapter
                .getAllMyActiveConversations(mBluetoothAdapter.getAddress());
        Contacts = Source;
        mContactsView = (ListView) v.findViewById(R.id.my_contacts_list);
        // mContactsView = activeConversationsFragment.Contacts_List;
        if (Contacts != null) {
            mContactsView.setAdapter(new ContactsInfoAdapter(this, Contacts
                    .size()));
            mContactsView.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1,
                                        int arg2, long arg3) {
                    // TODO Auto-generated method stub
                    String remoteMac = ((String) (Contacts.get(arg2)[1]));
                    setupChat(remoteMac);
                }
            });
        } else {
            String[] tmp = {"There are no recent conversations"};
            mContactsView.setAdapter(new ArrayAdapter<String>(this,
                    R.layout.basic_list_item_text, R.id.EmptyConv, tmp));
        }
        currenframe = 1;
        LT2 = new looperThread2();
        LT2.start();
    }

    public static looperThread2 LT2;
    public String currentOtherUser = "";

    private void setupChat(String OtherUser) {

        Log.d(TAG, "setupChat()");
        View v = PutInMainContentFrame(R.layout.chat);
        currentOtherUser = OtherUser;
        ArrayList<Object[]> Source = sqliteAdapter.getMessages(
                mBluetoothAdapter.getAddress(), OtherUser);
        mConversationView = (ListView) v.findViewById(R.id.conversation_list);
        chatData = Source;
        if (Source != null) {
            mConversationView.setAdapter(new chatAdapter(this, Source.size()));
        }
        mOutEditText = (EditText) v.findViewById(R.id.message_editor);
        mOutEditText.setOnEditorActionListener(mWriteListener);

        mSendButton = (Button) v.findViewById(R.id.send);

        mSendButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                String message = mOutEditText.getText().toString();
                long date = System.currentTimeMillis();
                if (!message.equals("")) {
                    mOutEditText.setText("");
                    sqliteAdapter.insertMessageDetails(
                            mBluetoothAdapter.getAddress(),
                            currentOtherUser, message,
                            date);

                    sqliteAdapter.getAllDataTrans();

                    if (internetConnection && admin.con.isConnected()) {
                        try {
                            admin.sendMessageNow(message, currentOtherUser, "" + date);
                        } catch (SmackException.NotConnectedException e) {
                            e.printStackTrace();
                        } catch (XMPPException e) {
                            e.printStackTrace();
                        }
                    }
                }

            }
        });
        sqliteAdapter.removeReadMessagesNotif(OtherUser);
        TextView textView = (TextView) findViewById(R.id.ContactName);
        textView.setText("" + sqliteAdapter.getContactInfo(currentOtherUser)[0]);
        currenframe = 3;
    }

    public Handler handler2 = new Handler();

    class looperThread2 extends Thread {
        @Override
        public void run() {
            handler2.postDelayed(new Runnable() {
                @Override
                public void run() {
                    FunctionHandler2();
                }
            }, 2500);
        }
    }

    int currenframe = 0;

    public void FunctionHandler2() {

        if (DBChanged) {
            switch (currenframe) {
                case 1: {
                    // recent
                    ArrayList<Object[]> Source = sqliteAdapter
                            .getAllMyActiveConversations(mBluetoothAdapter
                                    .getAddress());
                    if ((Source != null) && (Contacts.size() != Source.size())) {
                        Contacts = Source;
                        mContactsView.setAdapter(new ContactsInfoAdapter(this,
                                Contacts.size()));
                    }
                    break;
                }
                case 2: {
                    // contacts
                    ArrayList<Object[]> Source = sqliteAdapter.getContacts();
                    if ((Source != null) && (Contacts.size() != Source.size())) {
                        Contacts = Source;
                        mContactsView.setAdapter(new ContactsInfoAdapter(this,
                                Contacts.size()));
                    }
                    break;
                }
                case 3: {
                    // chat
                    ArrayList<Object[]> Source = sqliteAdapter.getMessages(
                            mBluetoothAdapter.getAddress(), currentOtherUser);
                    if ((Source != null)
                            && ((chatData == null) || (chatData.size() != Source
                            .size()))) {
                        chatData = Source;
                        mConversationView.setAdapter(new chatAdapter(this, Source
                                .size()));
                    }
                    break;
                }
            }
            sqliteAdapter.removeExeededMessages(3600000,
                    mBluetoothAdapter.getAddress());
            DBChanged = false;
        }
        String[] tmp = sqliteAdapter.UpdateCounters(tmp_SENT, tmp_RECIEVED);
        tmp_SENT = 0;
        tmp_RECIEVED = 0;
        updateStatus(Integer.parseInt(tmp[0]), Integer.parseInt(tmp[1]));
        LT2 = new looperThread2();
        LT2.start();
    }

    boolean isStopped = false;

    @Override
    public synchronized void onPause() {
        super.onPause();
        if (D)
            Log.e(TAG, "- ON PAUSE -");

    }

    @Override
    public void onStop() {
        super.onStop();
        if (D)
            Log.e(TAG, "-- ON STOP --");
        isStopped = true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Stop the Bluetooth chat services
        mChatServicesClient.stop();
        mChatServicesListener.stop();

        if (D)
            Log.e(TAG, "--- ON DESTROY ---");
    }

    private void ensureDiscoverable() {
        if (D)
            Log.d(TAG, "ensure discoverable");
        if (mBluetoothAdapter.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(
                    BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 0);
            startActivity(discoverableIntent);
        }
    }

    // The action listener for the EditText widget, to listen for the return key
    private TextView.OnEditorActionListener mWriteListener = new TextView.OnEditorActionListener() {
        public boolean onEditorAction(TextView view, int actionId,
                                      KeyEvent event) {
            // If the action is a key-up event on the return key, send the
            // message_remote
            if (actionId == EditorInfo.IME_NULL
                    && event.getAction() == KeyEvent.ACTION_UP) {
                String message = view.getText().toString();
                long date = System.currentTimeMillis();
                if (!message.equals("")) {
                    view.setText("");
                    // sendMessageClient(message_remote);
                    sqliteAdapter.insertMessageDetails(
                            mBluetoothAdapter.getAddress(),
                            currentOtherUser, message,
                            date);

                    sqliteAdapter.getAllDataTrans();


                    if (internetConnection && admin.con.isConnected()) {
                        try {
                            admin.sendMessageNow(message, currentOtherUser, "" + date);
                        } catch (SmackException.NotConnectedException e) {
                            e.printStackTrace();
                        } catch (XMPPException e) {
                            e.printStackTrace();
                        }
                    }
                }

            }
            if (D)
                Log.i(TAG, "END onEditorAction");
            return true;
        }
    };

    // The Handler that gets information back from the BluetoothChatService
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_STATE_CHANGE:
                    if (D)
                        Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                    switch (msg.arg1) {
                        case Client.STATE_CONNECTED_CLIENT:
                            // mConversationArrayAdapter.add("CONNECTED_CLIENT");
                            break;
                        case Client.STATE_CONNECTED_SERVER:
                            // mConversationArrayAdapter.add("CONNECTED_SERVER");

                            // }
                            break;
                        case Client.STATE_CONNECTING:
                            // mTitle.setText(R.string.title_connecting);
                            break;
                        case Client.STATE_LISTEN:
                            // mTitle.setText(R.string.title_not_connected);
                            break;
                        case Client.STATE_NONE:
                            break;
                    }
                    break;
                case MESSAGE_WRITE:
                    break;
                case MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;

                    converDataAsync dataAsync = new converDataAsync();
                    dataAsync.readBuf = readBuf;
                    dataAsync.start();

                    break;
                case MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    // mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    // Toast.makeText(getApplicationContext(), "Connected to ",
                    // Toast.LENGTH_SHORT).show();
                    break;
                case MESSAGE_TOAST:
                    // Toast.makeText(getApplicationContext(),
                    // msg.getData().getString(TOAST), Toast.LENGTH_SHORT)
                    // .show();
                    break;

                case CONNECTION_LOST:
                    // mConversationArrayAdapter.add("** lost connection **");

                    break;
            }
        }
    };

    public class converDataAsync extends Thread {
        byte[] readBuf;

        @Override
        public void run() {
            // TODO Auto-generated method stub
            super.run();
            AllData alldata = null;
            try {
                alldata = (AllData) Serializer.deserialize(Compression
                        .unpackRaw((readBuf)));
                // String[][] tmp = alldata.From_To_Message_Date;
            } catch (ClassNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            sqliteAdapter.insertTableTrans(alldata);
            BluCloud.tmp_RECIEVED++;
            this.interrupt();
        }

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (D)
            Log.d(TAG, "onActivityResult " + resultCode);
        switch (requestCode) {
            case REQUEST_ENABLE_BT: {
                if (resultCode == Activity.RESULT_OK) {
                    if (BSwitch != null)BSwitch.setText("Bluetooth Connected");
                    // PutInMainContentFrame(R.layout.chat);
                    ensureDiscoverable();
                    insertMyInfo(
                            "User"
                                    + mBluetoothAdapter.getAddress().replace(":",
                                    ""), "0", 0, null);
                    // setupRecentConversations();
                    // setupChat("example");
                    startProcessCycle();

                    sqliteAdapter.getAllDataTrans();
                    // setupChat();
                } else {
                    Log.d(TAG, "BT not enabled");
                    Toast.makeText(this, "BT is NOT enabled successfully",
                            Toast.LENGTH_SHORT).show();
                    if (BSwitch != null)BSwitch.setText("Bluetooth is NOT enabled successfully");
                    //finish();
                }
                break;
            }
        }
    }

    public void startProcessCycle() {
        // Initialize the BluetoothChatService to perform bluetooth connections
        if (currentBluThread == null) {
            if ((mChatServicesClient != null) && (mChatServicesClient != null)) {
                mChatServicesClient.stop();
                mChatServicesListener.stop();
            }
            mChatServicesClient = new Client(this, mHandler, mainBluCloud);
            mChatServicesListener = new listener(mainBluCloud, mHandler,
                    mainBluCloud);
            mNewDevices.clear();
            doDiscovery();
            currentBluThread = new looperThread();
            currentBluThread.start();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (DrawerListener.onOptionsItemSelected(item)) {
            return true;
        }
        return false;
    }

    Thread currentBluThread = null;

    public Handler handler = new Handler();
    public Runnable RunnableFunctionHandler = null;

    class looperThread extends Thread {
        @Override
        public void run() {
            RunnableFunctionHandler = new Runnable() {
                @Override
                public void run() {
                    FunctionHandler();
                }
            };
            handler.postDelayed(RunnableFunctionHandler, 10000);
        }
    }

    public void FunctionHandler() {

        mBluetoothAdapter.cancelDiscovery();
        mChatServicesListener.start();

        // DBAllData = sadapter.getAllData();
        // String[] test = DBAllData.split("BluCldBrk");

        // while (!mNewDevices.isEmpty()) {
        // String address = mNewDevices.remove(0)[1];

        // Get the BLuetoothDevice object
        // BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);

        // Attempt to connect to the device
        mChatServicesClient.connect(mNewDevices);
        // mConversationArrayAdapter.add("Connecting to  ... " + address);
        // }

        currentBluThread = new looperThread1();
        currentBluThread.start();
    }

    public Handler handler1 = new Handler();
    public Runnable RunnableFunctionHandler1 = null;

    class looperThread1 extends Thread {
        @Override
        public void run() {
            RunnableFunctionHandler1 = new Runnable() {
                @Override
                public void run() {
                    FunctionHandler1();
                }
            };
            handler1.postDelayed(RunnableFunctionHandler1, 50000);
        }
    }

    public void FunctionHandler1() {
        // mChatServicesListener.stop();
        // mChatServicesClient.stop();
        mChatServicesClient.clearCurrentConnections();
        mChatServicesListener.clearCurrentConnections();
        mNewDevices.clear();
        doDiscovery();
        currentBluThread = new looperThread();
        currentBluThread.start();
    }

    private void doDiscovery() {
        if (D)
            Log.d(TAG, "doDiscovery()");

        // If we're already discovering, stop it
        if (mBluetoothAdapter.isDiscovering()) {
            mBluetoothAdapter.cancelDiscovery();
        }

        // Request discover from BluetoothAdapter

        mBluetoothAdapter.startDiscovery();
        // mConversationArrayAdapter.add("Discovering ...");
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent
                        .getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // If it's already paired, skip it, because it's been listed
                // already
                for (int i = 0; i < mNewDevices.size(); i++) {
                    if (mNewDevices.get(i).getAddress()
                            .equals(device.getAddress())) {
                        // mConversationArrayAdapter
                        // .add("already added to the new devices list");
                        return;
                    }
                }

                // String names[] = { device.getName(), device.getAddress() };
                mNewDevices.add(device);
                // mConversationArrayAdapter.add("Added :- " + device.getName()
                // + device.getAddress());

                // When discovery is finished, change the Activity title
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED
                    .equals(action)) {

            }
        }
    };
    boolean Quit = false;

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        if (currenframe != 3) {
            if (Quit) {
                super.onBackPressed();
            } else {
                Quit = true;
                CancelQuit cq = new CancelQuit();
                Toast.makeText(
                        this,
                        "Are you sure you want to exit ? \n Hint : ( click on the 'Upper Left' button to navigate )",
                        Toast.LENGTH_LONG).show();
                cq.start();
            }
        } else {
            setupRecentConversations();
        }

        //

    }

    public Handler handler3 = new Handler();

    class CancelQuit extends Thread {
        @Override
        public void run() {
            handler3.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Quit = false;
                }
            }, 1000);
        }
    }

    public class OptionsItemsAdapter extends ArrayAdapter<String> {
        Context c;
        ArrayList<Object[]> data;

        public OptionsItemsAdapter(Context context) {
            super(context, R.layout.options_item, R.id.OptionDesc,
                    new String[4]);
            // TODO Auto-generated constructor stub
            c = context;
            ArrayList<Object[]> tmp = new ArrayList<Object[]>();
            Object[] obj1 = {"recent", R.drawable.recent};
            tmp.add(obj1);
            Object[] obj2 = {"Contacts", R.drawable.contactsicon};
            tmp.add(obj2);
            Object[] obj3 = {"Settings", R.drawable.settings};
            tmp.add(obj3);
            Object[] obj4 = {"About", R.drawable.about};
            tmp.add(obj4);
            this.data = tmp;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            View row = convertView;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) c
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.options_item, parent, false);
            }
            TextView OptionDesc = (TextView) row.findViewById(R.id.OptionDesc);
            ImageView image = (ImageView) row.findViewById(R.id.OptionImage);
            Object[] tmp = data.get(position);
            String SName = (String) tmp[0];
            int IImage = (Integer) tmp[1];
            // byte[] BImage = (byte[]) tmp[1];
            OptionDesc.setText(SName);
            image.setImageResource(IImage);
            return row;
        }
    }

    public class ContactsInfoAdapter extends ArrayAdapter<String> {
        Context c;

        public ContactsInfoAdapter(Context context, int len) {
            super(context, R.layout.row_contact_info, R.id.ContactName,
                    new String[len]);
            // TODO Auto-generated constructor stub
            c = context;
            // contactsData = data;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            View row = convertView;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) c
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater
                        .inflate(R.layout.row_contact_info, parent, false);
            }
            TextView Name = (TextView) row.findViewById(R.id.ContactName);
            TextView Desc = (TextView) row.findViewById(R.id.Description);
            ImageView image = (ImageView) row.findViewById(R.id.ContactImage);
            Object[] tmp = Contacts.get(position);
            String SName = "" + tmp[0];
            String SMac = "" + tmp[1];
            String BImage = "" + tmp[2];
            Name.setText(SName);
            //recent chat adapter
            if (tmp.length > 3) {
                String UnRead = "" + tmp[3];
                if (UnRead.equals("1")) {
                    Name.setText(SName + " => new messages <=");
                }
            }


            Desc.setText(SMac);
            if ((BImage != null) && !BImage.equals("null") && (!BImage.equals("0"))) {
                image.setImageResource(getImageByNum(Integer.parseInt(BImage)));
            }
            return row;
        }
    }

    ArrayList<Object[]> chatData = new ArrayList<Object[]>();

    public class chatAdapter extends ArrayAdapter<String> {
        Context c;
        String myAdress;
        String remoteAdress;

        public chatAdapter(Context context, int Len) {
            super(context, R.layout.message_remote, R.id.message, new String[Len]);
            // TODO Auto-generated constructor stub
            c = context;
            // chatData = data;
            myAdress = mBluetoothAdapter.getAddress();
            remoteAdress = currentOtherUser;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            Object[] tmp = chatData.get(position);
            String Smessage = (String) tmp[2];
            String BImage = (String) tmp[6];
            String LocalOrRemote = (String) tmp[7];

            View row = convertView;
            myViewHolder holder = null;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) c
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                if (LocalOrRemote.equals("local")) {
                    row = inflater.inflate(R.layout.message_local, parent, false);
                } else {
                    row = inflater.inflate(R.layout.message_remote, parent, false);
                }
                holder = new myViewHolder(row);
                row.setTag(holder);
            } else {
                holder = (myViewHolder) row.getTag();
            }

            if (holder.image != null && (BImage != null) && (!BImage.equals("0"))) {
                holder.image.setImageResource(getImageByNum(Integer
                        .parseInt(BImage)));
            }
            holder.message.setText(Smessage);
            // TextView message_remote = (TextView) row.findViewById(R.id.message_remote);
            // ImageView image = (ImageView) row.findViewById(R.id.picture);

            // String SFrom = (String) tmp[0];
            // String Date = (String) tmp[3];
            /*
             * message_remote.setText(Smessage);
			 * 
			 * if ((BImage != null) && (!BImage.equals("0"))) {
			 * image.setImageResource()); }
			 */
            // if (myAdress.equals(SFrom)){
            // bImage
            // }else{
            // bImage =
            // BitmapFactory.decodeByteArray(remoteUserImage,-1,remoteUserImage.length);
            // }

            return row;
        }

        public class myViewHolder {
            TextView message;
            ImageView image;

            public myViewHolder(View v) {
                message = (TextView) v.findViewById(R.id.message);
                image = (ImageView) v.findViewById(R.id.picture);
            }
        }
    }

    public class MyInfoAdapter extends ArrayAdapter<String> {
        Context c;
        ArrayList<Integer> ImagesData;

        public MyInfoAdapter(Context context) {
            super(context, R.layout.row_images_my_info, 0, new String[10]);
            // TODO Auto-generated constructor stub
            c = context;
            ArrayList<Integer> tmp = new ArrayList<Integer>();
            tmp.add(R.drawable.a11);
            tmp.add(R.drawable.a12);
            tmp.add(R.drawable.a21);
            tmp.add(R.drawable.a22);
            tmp.add(R.drawable.a31);
            tmp.add(R.drawable.a32);
            tmp.add(R.drawable.a41);
            tmp.add(R.drawable.a42);
            tmp.add(R.drawable.a51);
            tmp.add(R.drawable.a52);
            this.ImagesData = tmp;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            View row = convertView;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) c
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.row_images_my_info, parent,
                        false);
            }
            ImageView image = (ImageView) row.findViewById(R.id.PreviewImage);
            int tmp = ImagesData.get(position);
            image.setImageResource(tmp);
            return row;
        }
    }
}