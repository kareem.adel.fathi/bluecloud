package com.blucloud.kareemadelfathi.blucloud;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class SqliteAdapter {

    sqlhelper sqlhlpr;
    BluCloud Parent;

    public SqliteAdapter(Context context, BluCloud Parent) {
        sqlhlpr = new sqlhelper(context);
        this.Parent = Parent;
    }

    public class insertTableAsync extends Thread {
        AllData alldata;

        @Override
        public void run() {
            // TODO Auto-generated method stub
            super.run();
            try {
                insertTable(alldata);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            this.interrupt();
        }

    }

    public void insertTableTrans(AllData alldata) {
        insertTableAsync tableAsync = new insertTableAsync();
        tableAsync.alldata = alldata;
        tableAsync.start();
    }

    public synchronized void insertTable(AllData alldata) throws IOException {
        if (alldata != null) {
            if (alldata.From_To_Message_Date != null) {
                int len = alldata.From_To_Message_Date[0].length;
                long DateC;
                long[] dates = new long[len];
                for (int i = 0; i < len; i++) {
                    DateC = (Long) alldata.From_To_Message_Date[3][i];
                    dates[i] = DateC;
                }
                long[] missingRecords = null;
                if (dates.length != 0) {
                    missingRecords = getDataByDate(dates);
                }
                if (missingRecords != null) {
                    Arrays.sort(missingRecords);
                    for (int i = 0; i < len; i++) {
                        DateC = (Long) alldata.From_To_Message_Date[3][i];
                        // long test;
                        if (((Arrays.binarySearch(missingRecords, DateC)) < 0)
                                || DateC == 0)
                            continue;
                        insertMessageDetails(
                                (String) alldata.From_To_Message_Date[0][i],
                                (String) alldata.From_To_Message_Date[1][i],
                                (String) alldata.From_To_Message_Date[2][i],
                                DateC);
                    }
                }
            }
            if (alldata.Name_Mac_Phone_Date_pic != null) {
                int len = alldata.Name_Mac_Phone_Date_pic[0].length;
                for (int i = 0; i < len; i++) {
                    String Mac = (String) alldata.Name_Mac_Phone_Date_pic[1][i];
                    Object[] currentContact = getContactInfo(Mac);
                    if ((currentContact != null)) {
                        Object[] contactInfo = currentContact;
                        long LInDB = (Long) contactInfo[3];
                        long LRemote = (Long) alldata.Name_Mac_Phone_Date_pic[3][i];
                        if (LInDB < LRemote) {
                            UpdateContactsDetails(
                                    (String) alldata.Name_Mac_Phone_Date_pic[0][i],
                                    (String) alldata.Name_Mac_Phone_Date_pic[1][i],
                                    (String) alldata.Name_Mac_Phone_Date_pic[2][i],
                                    (Long) alldata.Name_Mac_Phone_Date_pic[3][i],
                                    (String) alldata.Name_Mac_Phone_Date_pic[4][i]);
                        }
                        continue;
                    }
                    insertContactDetails(
                            (String) alldata.Name_Mac_Phone_Date_pic[0][i],
                            (String) alldata.Name_Mac_Phone_Date_pic[1][i],
                            (String) alldata.Name_Mac_Phone_Date_pic[2][i],
                            (Long) alldata.Name_Mac_Phone_Date_pic[3][i],
                            (String) alldata.Name_Mac_Phone_Date_pic[4][i]);
                }
            }
            if (BluCloud.DBChanged) {
                // getAllData();
                getAllDataAsync allDataAsync = new getAllDataAsync();
                allDataAsync.start();

            }
        }
    }

    public double UpdateContactsDetails(String Name, String Mac, String Phone,
                                        long Date, String myChosenImage) {
        SQLiteDatabase db = sqlhlpr.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(sqlhelper.Name, Name);
        contentValues.put(sqlhelper.MacAddress, Mac);
        contentValues.put(sqlhelper.phoneNumber, Phone);
        contentValues.put(sqlhelper.Date, Date);
        contentValues.put(sqlhelper.image, myChosenImage);
        double ret = db.update(sqlhelper.contactsTable, contentValues,
                sqlhelper.MacAddress + " = '" + Mac + "'", null);
        /*
         * try { getAllData(); } catch (IOException e) { // TODO Auto-generated
		 * catch block e.printStackTrace(); }
		 */
        BluCloud.DBChanged = true;

        return ret;
    }

    public synchronized void UpdateCurrentIp(String IP) {
        SQLiteDatabase db = sqlhlpr.getWritableDatabase();
        db.execSQL("UPDATE " + sqlhelper.LastIpTable + " SET "
                + sqlhelper.LastIpValue + " = '"
                + IP + "' WHERE " + sqlhelper.LastIp + " = " + "'IP'");
    }

    public synchronized String GetCurrentIp() {
        SQLiteDatabase db = sqlhlpr.getWritableDatabase();
        String[] colums = {sqlhelper.LastIpValue};
        Cursor cursor = db.query(sqlhelper.LastIpTable, colums, null, null,
                null, null, null);
        String ret = "";
        while (cursor.moveToNext()) {
            String type = cursor.getString(cursor
                    .getColumnIndex(sqlhelper.LastIpValue));
            ret = type;
        }
        cursor.close();
        return ret;
    }

    public synchronized String[] UpdateCounters(int sent, int recieved) {
        SQLiteDatabase db = sqlhlpr.getWritableDatabase();
        db.execSQL("UPDATE " + sqlhelper.countersTable + " SET "
                + sqlhelper.TypeValue + " = " + sqlhelper.TypeValue + " +"
                + sent + " WHERE " + sqlhelper.Type + " = " + "'sent'");
        db.execSQL("UPDATE " + sqlhelper.countersTable + " SET "
                + sqlhelper.TypeValue + " = " + sqlhelper.TypeValue + " +"
                + recieved + " WHERE " + sqlhelper.Type + " = " + "'recieved'");

        String[] colums = {sqlhelper.Type, sqlhelper.TypeValue};
        Cursor cursor = db.query(sqlhelper.countersTable, colums, null, null,
                null, null, null);
        String ret[] = new String[2];
        while (cursor.moveToNext()) {
            String type = cursor.getString(cursor
                    .getColumnIndex(sqlhelper.Type));
            if (type.equals("sent")) {
                ret[0] = cursor.getString(cursor
                        .getColumnIndex(sqlhelper.TypeValue));
            } else {
                ret[1] = cursor.getString(cursor
                        .getColumnIndex(sqlhelper.TypeValue));
            }

        }
        cursor.close();
        return ret;
    }

    public double insertContactDetails(String Name, String Mac, String Phone,
                                       long Date, String myChosenImage) {
        SQLiteDatabase db = sqlhlpr.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(sqlhelper.Name, Name);
        contentValues.put(sqlhelper.MacAddress, Mac);
        contentValues.put(sqlhelper.phoneNumber, Phone);
        contentValues.put(sqlhelper.Date, Date);
        contentValues.put(sqlhelper.image, myChosenImage);

        double ret = db.insert(sqlhelper.contactsTable, null, contentValues);
        /*
         * try { getAllData(); } catch (IOException e) { // TODO Auto-generated
		 * catch block e.printStackTrace(); }
		 */
        BluCloud.DBChanged = true;

        return ret;
    }

    public double insertMessageDetails(String From, String To, String Message,
                                       long Date) {

        SQLiteDatabase db = sqlhlpr.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(sqlhelper.From, From);
        contentValues.put(sqlhelper.To, To);
        try {
            contentValues.put(sqlhelper.MessageData, Compression.encryptStr(Message));
        } catch (IOException e) {
            e.printStackTrace();
        }
        contentValues.put(sqlhelper.Date, Date);

        double ret = db.insert(sqlhelper.messagesTable, null, contentValues);

        BluCloud.DBChanged = true;

        if ((Parent.isStopped && To.equals(Parent.mBluetoothAdapter.getAddress()))
                || (!Parent.isStopped && To.equals(Parent.mBluetoothAdapter.getAddress()) && !From.equals(Parent.currentOtherUser))) {
            Object[] contactInfo = getContactInfo(From);
            if (contactInfo == null) {
                Parent.createNotification("BluCloud", From + " sent you a message_remote", "BluCloud has received a message_remote", "Details");
            } else {
                Parent.createNotification("BluCloud", contactInfo[0] + " sent you a message_remote", "BluCloud has received a message_remote", "Details");
            }
            contentValues.clear();
            contentValues = new ContentValues();
            contentValues.put(sqlhelper.msgNotifContactName, From);

            db.insertWithOnConflict(sqlhelper.msgNotifTable, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
        }

        return ret;
    }

    public int removeExeededMessages(long allowedTimeToKeepMessages,
                                     String currentUser) {
        SQLiteDatabase db = sqlhlpr.getWritableDatabase();
        int ret = db.delete(sqlhelper.messagesTable, sqlhelper.Date + " < "
                        + (System.currentTimeMillis() - allowedTimeToKeepMessages)
                        + " AND " + sqlhelper.From + " != " + "'" + currentUser + "'"
                        + " AND " + sqlhelper.To + " != " + "'" + currentUser + "'",
                null);
        return ret;

    }

    public long[] getDataByDate(long[] Dates) {
        String condition = "";
        for (int i = 0; i < Dates.length - 1; i++) {
            condition += sqlhelper.Date + " = " + Dates[i] + " OR ";
        }
        condition += sqlhelper.Date + " = " + Dates[Dates.length - 1];
        SQLiteDatabase db = sqlhlpr.getWritableDatabase();
        // String[] colums = { SQLDB.From, SQLDB.To,
        // SQLDB.MessageData, SQLDB.Date };
        String[] colums = {sqlhelper.Date};
        Cursor cursor = db.query(sqlhelper.messagesTable, colums, condition,
                null, null, null, sqlhelper.Date);
        if (cursor.getCount() == 0) {
            cursor.close();
            return Dates;
        }
        if (cursor.getCount() >= Dates.length) {
            return null;
        }
        long[] ret = new long[cursor.getCount()];
        int i = 0;
        while (cursor.moveToNext()) {
            long date = cursor.getLong(cursor.getColumnIndex(sqlhelper.Date));
            ret[i] = date;
            i++;
        }
        Arrays.sort(ret);
        Arrays.sort(Dates);
        long[] retFinal = new long[Dates.length - cursor.getCount() + 1];
        int z = 0;
        for (int x = 0; x < Dates.length; x++) {
            if (Arrays.binarySearch(ret, Dates[x]) < 0) {
                retFinal[z] = Dates[x];
                z++;
            }
        }
        cursor.close();
        return retFinal;
    }

    public boolean stringArrayContains(String[] y, String x) {
        for (int i = 0; i < y.length; i++) {
            if (y[i].equals(x)) {
                return true;
            }
        }
        return false;
    }

    public ArrayList<Object[]> getMessages(String CurrentUser, String OtherUser) {
        Object[] contactInfoCurrentCheck = getContactInfo(CurrentUser);
        String CurrentUserName = null;
        String CurrentUserMac = null;
        String CurrentUserImage = null;
        if (contactInfoCurrentCheck != null) {
            Object[] contactInfoCurrent = contactInfoCurrentCheck;
            Object[] tmp = contactInfoCurrent;
            CurrentUserName = (String) tmp[0];
            CurrentUserMac = (String) tmp[1];
            CurrentUserImage = (String) tmp[4];
        }
        Object[] contactInfoOtherCheck = getContactInfo(OtherUser);
        String OtherUserName = null;
        String OtherUserMac = null;
        String OtherUserImage = null;
        if (contactInfoOtherCheck != null) {
            Object[] contactInfoOther = contactInfoOtherCheck;
            Object[] tmp = contactInfoOther;
            OtherUserName = (String) tmp[0];
            OtherUserMac = (String) tmp[1];
            OtherUserImage = (String) tmp[4];
        }

        SQLiteDatabase db = sqlhlpr.getWritableDatabase();
        String[] colums = {sqlhelper.From, sqlhelper.To,
                sqlhelper.MessageData, sqlhelper.Date};
        Cursor cursor = db
                .query(sqlhelper.messagesTable, colums, "(" + sqlhelper.To
                                + " = '" + CurrentUser + "' AND " + sqlhelper.From
                                + " = '" + OtherUser + "') OR (" + sqlhelper.To
                                + " = '" + OtherUser + "' AND " + sqlhelper.From
                                + " = '" + CurrentUser + "')", null, null, null,
                        sqlhelper.Date);
        if (cursor.getCount() == 0) {
            cursor.close();
            return null;
        }
        ArrayList<Object[]> al = new ArrayList<Object[]>();
        while (cursor.moveToNext()) {
            /*
             * String From = cursor.getString(cursor
			 * .getColumnIndex(SQLDB.From)); String To =
			 * cursor.getString(cursor.getColumnIndex(SQLDB.To)); String
			 * message_remote = cursor.getString(cursor
			 * .getColumnIndex(SQLDB.MessageData)); String date =
			 * cursor.getString(cursor .getColumnIndex(SQLDB.Date));
			 * String[] tmp = { From, To, message_remote, date }; al.add(tmp);
			 */
            String From = cursor.getString(cursor
                    .getColumnIndex(sqlhelper.From));
            String To = cursor.getString(cursor.getColumnIndex(sqlhelper.To));
            byte[] Bmessage = cursor.getBlob(cursor
                    .getColumnIndex(sqlhelper.MessageData));
            String message = null;
            if (Bmessage != null) {
                try {
                    message = Compression.decryptStr(Bmessage);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            long Date = cursor.getLong(cursor.getColumnIndex(sqlhelper.Date));
            Object[] tmp = new Object[8];
            if (From.equals(CurrentUser)) {
                tmp[0] = From;
                tmp[1] = To;
                tmp[2] = message;
                tmp[3] = Date;
                tmp[4] = CurrentUserName;
                tmp[5] = CurrentUserMac;
                tmp[6] = CurrentUserImage;
                tmp[7] = "local";
            } else {
                tmp[0] = From;
                tmp[1] = To;
                tmp[2] = message;
                tmp[3] = Date;
                tmp[4] = OtherUserName;
                tmp[5] = OtherUserMac;
                tmp[6] = OtherUserImage;
                tmp[7] = "remote";
            }
            al.add(tmp);
        }
        cursor.close();

        return al;
    }

    public ArrayList<Object[]> getContacts() {
        SQLiteDatabase db = sqlhlpr.getWritableDatabase();
        String[] colums = {sqlhelper.Name, sqlhelper.MacAddress,
                sqlhelper.image};
        Cursor cursor = db.query(sqlhelper.contactsTable, colums, null, null,
                null, null, sqlhelper.Name);
        if (cursor.getCount() == 0) {
            cursor.close();
            return null;
        }
        ArrayList<Object[]> al = new ArrayList<Object[]>();
        // int i = 0;
        while (cursor.moveToNext()) {
            String Name = cursor.getString(cursor
                    .getColumnIndex(sqlhelper.Name));
            String Mac = cursor.getString(cursor
                    .getColumnIndex(sqlhelper.MacAddress));
            String Image = cursor.getString(cursor
                    .getColumnIndex(sqlhelper.image));
            Object[] tmp = {Name, Mac, Image};
            al.add(tmp);
        }
        cursor.close();
        return al;
    }

    public ArrayList<Object[]> getAllMyActiveConversations(String CurrentUser) {
        SQLiteDatabase db = sqlhlpr.getWritableDatabase();
        String[] colums = {sqlhelper.From, sqlhelper.To};

        Cursor cursor = db.query(true, sqlhelper.messagesTable, colums, sqlhelper.To
                + " = '" + CurrentUser + "' OR " + sqlhelper.From + " = '"
                + CurrentUser + "'", null, null, null, sqlhelper.Date, null);
        if (cursor.getCount() == 0) {
            cursor.close();
            return null;
        }
        String[] s = new String[cursor.getCount()];
        Arrays.fill(s, "");
        int i = 0;
        while (cursor.moveToNext()) {
            String From = cursor.getString(cursor
                    .getColumnIndex(sqlhelper.From));
            String To = cursor.getString(cursor.getColumnIndex(sqlhelper.To));

            if (!From.equals(CurrentUser)) {
                if (!stringArrayContains(s, From)) {
                    s[i] = From;
                } else {
                    continue;
                }
                //s[i] = From;
            } else {
                if (!stringArrayContains(s, To)) {
                    s[i] = To;
                } else {
                    continue;
                }
                //s[i] = To;
            }

            i++;
        }
        String[] ret = new String[i];
        i = 0;
        while ((i != s.length) && (!s[i].equals(""))) {
            ret[i] = s[i];
            i++;
        }

        String[] UnReadMacs = getUnReadMessagesNotif();
        if (UnReadMacs != null) {
            Arrays.sort(UnReadMacs);
        }

        ArrayList<Object[]> al = new ArrayList<Object[]>();

        for (int a = 0; a < ret.length; a++) {
            Object[] tmp1 = getContactInfo(ret[a]);
            Object[] tmp = new Object[4];
            tmp[0] = tmp1[0];
            tmp[1] = tmp1[1];
            tmp[2] = tmp1[4];
            if (UnReadMacs != null) {
                if (Arrays.binarySearch(UnReadMacs, tmp1[1]) >= 0) {
                    tmp[3] = "1";
                } else {
                    tmp[3] = "0";
                }
            } else {
                tmp[3] = "0";
            }

            al.add(tmp);
        }
        if (al.isEmpty()) {
            cursor.close();
            return null;
        }
        cursor.close();
        return al;
    }

    public void removeReadMessagesNotif(String MAC) {
        SQLiteDatabase db = sqlhlpr.getWritableDatabase();
        db.delete(sqlhelper.msgNotifTable, sqlhelper.msgNotifContactName + " = '" + MAC + "'", null);
    }

    public String[] getUnReadMessagesNotif() {
        SQLiteDatabase db = sqlhlpr.getWritableDatabase();
        String[] colums = {sqlhelper.msgNotifContactName};
        Cursor cursor = db.query(sqlhelper.msgNotifTable, colums,
                null, null, null,
                null, null);
        if (cursor.getCount() == 0) {
            cursor.close();
            return null;
        }
        String[] unread = new String[cursor.getCount()];
        int i = 0;
        while (cursor.moveToNext()) {
            String MAC = cursor.getString(cursor
                    .getColumnIndex(sqlhelper.msgNotifContactName));

            unread[i] = (MAC);
            i++;
        }
        cursor.close();
        return unread;
    }

    public Object[] getContactInfo(String ContactMac) {
        SQLiteDatabase db = sqlhlpr.getWritableDatabase();
        String[] colums = {sqlhelper.Name, sqlhelper.MacAddress,
                sqlhelper.phoneNumber, sqlhelper.Date, sqlhelper.image};
        Cursor cursor = db.query(sqlhelper.contactsTable, colums,
                sqlhelper.MacAddress + " = '" + ContactMac + "'", null, null,
                null, null);
        if (cursor.getCount() == 0) {
            cursor.close();
            //return new Object[]{ContactMac, ContactMac, Long.valueOf(0), Long.valueOf(0), Long.valueOf(0)};
            return null;
        }
        Object[] al = null;
        cursor.moveToNext();
        String Name = cursor.getString(cursor
                .getColumnIndex(sqlhelper.Name));
        String MacAddress = cursor.getString(cursor
                .getColumnIndex(sqlhelper.MacAddress));
        String image = cursor.getString(cursor
                .getColumnIndex(sqlhelper.image));
        String Phone = cursor.getString(cursor
                .getColumnIndex(sqlhelper.phoneNumber));
        long Date = cursor.getLong(cursor.getColumnIndex(sqlhelper.Date));
        Object[] tmp = {Name, MacAddress, Phone, Date, image};
        al = (tmp);

        cursor.close();
        return al;
    }

    public class getAllDataAsync extends Thread {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            super.run();
            try {
                getAllData();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            this.interrupt();
        }

    }

    public void getAllDataTrans() {
        getAllDataAsync AllDataAsync = new getAllDataAsync();
        AllDataAsync.start();
    }

    public synchronized byte[] getAllData() throws IOException {
        SQLiteDatabase db = sqlhlpr.getWritableDatabase();
        // db.
        String[] colums = {sqlhelper.From, sqlhelper.To,
                sqlhelper.MessageData, sqlhelper.Date};
        Cursor cursor = db.query(sqlhelper.messagesTable, colums, (System.currentTimeMillis() + " - " + sqlhelper.Date + " < 604800000"//sev en days
                ), null,
                null, null, null);
        AllData ret = new AllData();
        Object[][] tmp_From_To_Message_Date = new Object[4][cursor.getCount()];
        int i = 0;
        if (cursor.getCount() != 0) {
            while (cursor.moveToNext()) {
                String From = cursor.getString(cursor
                        .getColumnIndex(sqlhelper.From));
                String To = cursor.getString(cursor
                        .getColumnIndex(sqlhelper.To));
                //String message = cursor.getString(cursor
                //       .getColumnIndex(sqlhelper.MessageData));
                byte[] Bmessage = cursor.getBlob(cursor
                        .getColumnIndex(sqlhelper.MessageData));
                String message = null;
                if (Bmessage != null) {
                    try {
                        message = Compression.decryptStr(Bmessage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                long Date = cursor.getLong(cursor
                        .getColumnIndex(sqlhelper.Date));
                tmp_From_To_Message_Date[0][i] = From;
                tmp_From_To_Message_Date[1][i] = To;
                tmp_From_To_Message_Date[2][i] = message;
                tmp_From_To_Message_Date[3][i] = Date;
                i++;
            }
        }

        String[] colums1 = {sqlhelper.Name, sqlhelper.MacAddress,
                sqlhelper.phoneNumber, sqlhelper.Date, sqlhelper.image};
        cursor = db.query(sqlhelper.contactsTable, colums1, null, null, null,
                null, null);
        Object[][] tmp_Name_Mac_Phone_Date_pic = new Object[5][cursor
                .getCount()];
        if (cursor.getCount() != 0) {
            i = 0;
            while (cursor.moveToNext()) {
                String Name = cursor.getString(cursor
                        .getColumnIndex(sqlhelper.Name));
                String Mac = cursor.getString(cursor
                        .getColumnIndex(sqlhelper.MacAddress));
                String Phone = cursor.getString(cursor
                        .getColumnIndex(sqlhelper.phoneNumber));
                long Date = cursor.getLong(cursor
                        .getColumnIndex(sqlhelper.Date));
                String image = cursor.getString(cursor
                        .getColumnIndex(sqlhelper.image));
                tmp_Name_Mac_Phone_Date_pic[0][i] = Name;
                tmp_Name_Mac_Phone_Date_pic[1][i] = Mac;
                tmp_Name_Mac_Phone_Date_pic[2][i] = Phone;
                tmp_Name_Mac_Phone_Date_pic[3][i] = Date;
                tmp_Name_Mac_Phone_Date_pic[4][i] = image;
                i++;
            }
        }
        ret.From_To_Message_Date = tmp_From_To_Message_Date;
        ret.Name_Mac_Phone_Date_pic = tmp_Name_Mac_Phone_Date_pic;
        byte[] AllRet = Compression.packRaw(Serializer.serialize(ret));
        BluCloud.DBAllData = AllRet;
        cursor.close();
        return AllRet;
    }

    class sqlhelper extends SQLiteOpenHelper {
        private static final String DatabaseName = "BluDatabase.db";
        private static final String messagesTable = "messages";
        private static final String contactsTable = "contacts";
        private static final String countersTable = "counters";
        private static final String LastIpTable = "LastIpTable";
        private static final String LastIp = "LastIp";
        private static final String LastIpValue = "Value";
        private static final String msgNotifTable = "msgNotif";
        private static final String msgNotifContactName = "ContactName";
        private static final String Type = "sent";
        private static final String TypeValue = "TypeValue";
        private static final String From = "fromUser";
        private static final String To = "toUser";
        private static final String MessageData = "message";
        private static final String Date = "date";
        private static final String Name = "Name";
        private static final String MacAddress = "Mac";
        private static final String image = "image";
        private static final String phoneNumber = "phoneNumber";
        // private static final String DecompressedSize = "DecompressedSize";
        private static final String id = "_id";
        private static final int version = 28;

        // private Context context;

        public sqlhelper(Context context) {
            super(context, DatabaseName, null, version);
            // TODO Auto-generated constructor stub
            // this.context = context;
            // message_remote.show(context, "Constructor !");
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // TODO Auto-generated method stub
            try {
                db.execSQL("CREATE TABLE " + messagesTable + " (" + id
                        + " INTEGER PRIMARY KEY AUTOINCREMENT, " + From
                        + " TEXT, " + To + " TEXT, " + MessageData + " TEXT, "
                        + Date + " INTEGER);");
                db.execSQL("CREATE TABLE " + contactsTable + " (" + id
                        + " INTEGER PRIMARY KEY AUTOINCREMENT, " + Name
                        + " TEXT, " + MacAddress + " TEXT, " + phoneNumber
                        + " TEXT, " + Date + " INTEGER, " + image + " TEXT);");
                db.execSQL("CREATE TABLE " + countersTable + " (" + id
                        + " INTEGER PRIMARY KEY AUTOINCREMENT, " + Type
                        + " TEXT, " + TypeValue + " TEXT);");
                db.execSQL("CREATE TABLE " + msgNotifTable + " (" + id
                        + " INTEGER PRIMARY KEY AUTOINCREMENT, " + msgNotifContactName
                        + " TEXT NOT NULL UNIQUE);");
                db.execSQL("CREATE TABLE " + LastIpTable + " (" + id
                        + " INTEGER PRIMARY KEY AUTOINCREMENT, " + LastIp
                        + " TEXT, " + LastIpValue + " TEXT);");

                ContentValues contentValues = new ContentValues();
                contentValues.put(sqlhelper.Type, "sent");
                contentValues.put(sqlhelper.TypeValue, "0");
                db.insert(sqlhelper.countersTable, null, contentValues);
                contentValues.clear();
                contentValues.put(sqlhelper.Type, "recieved");
                contentValues.put(sqlhelper.TypeValue, "0");
                db.insert(sqlhelper.countersTable, null, contentValues);
                contentValues.clear();
                contentValues.put(sqlhelper.LastIp, "IP");
                contentValues.put(sqlhelper.LastIpValue, "197.35.51.221");
                db.insert(sqlhelper.LastIpTable, null, contentValues);
                contentValues.clear();

                // message_remote.show(context, "Table created !");
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
            // TODO Auto-generated method stub
            try {
                db.execSQL("DROP TABLE IF EXISTS " + messagesTable);
                db.execSQL("DROP TABLE IF EXISTS " + contactsTable);
                db.execSQL("DROP TABLE IF EXISTS " + countersTable);
                db.execSQL("DROP TABLE IF EXISTS " + msgNotifTable);

                onCreate(db);
                // message_remote.show(context, "Table created !");
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

}
