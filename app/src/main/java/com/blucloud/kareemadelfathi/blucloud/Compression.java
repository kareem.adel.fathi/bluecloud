package com.blucloud.kareemadelfathi.blucloud;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class Compression {
    public static byte[] packRaw(byte[] b) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        GZIPOutputStream zos = new GZIPOutputStream(baos);
        zos.write(b);
        zos.close();

        return baos.toByteArray();
    }

    public static byte[] unpackRaw(byte[] b) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ByteArrayInputStream bais = new ByteArrayInputStream(b);

        GZIPInputStream zis = new GZIPInputStream(bais);
        byte[] tmpBuffer = new byte[256];
        int n;
        while ((n = zis.read(tmpBuffer)) >= 0)
            baos.write(tmpBuffer, 0, n);
        zis.close();

        return baos.toByteArray();
    }

    private static byte[] Key = "7LL=.-qK|tfM;^t".getBytes();

    public static byte[] encryptStr(String s) throws IOException {
        byte[] key = Key;
        byte[] b = s.getBytes();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        for (int i = 0; i < b.length; i++) {
            baos.write(b[i] ^ key[i % key.length]);
        }
        return (baos.toByteArray());
    }

    public static String decryptStr(byte[] b) throws IOException {
        byte[] key = Key;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        for (int i = 0; i < b.length; i++) {
            baos.write(b[i] ^ key[i % key.length]);
        }
        return new String(baos.toByteArray());
    }

}
