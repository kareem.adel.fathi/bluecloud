/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blucloud.kareemadelfathi.blucloud;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.UUID;

/**
 * This class does all the work for setting up and managing Bluetooth
 * connections with other devices. It has a thread that listens for incoming
 * connections, a thread for connecting with a device, and a thread for
 * performing data transmissions when connected.
 */
public class Client {
    // Debugging
    private static final String TAG = "BluetoothChatService";
    private static final boolean D = true;

    private String[] remoteBluetoothName = {"null", "null"};

    // Unique UUID for this application
    private static final UUID[] MY_UUID = new UUID[]{
            UUID.fromString("0bacf314-4da3-11e4-9e35-164230d1df67"),
            UUID.fromString("0bacf756-4da3-11e4-9e35-164230d1df67"),
            UUID.fromString("0bacf8b4-4da3-11e4-9e35-164230d1df67"),
            UUID.fromString("0bacf9ea-4da3-11e4-9e35-164230d1df67"),
            UUID.fromString("0bacfb20-4da3-11e4-9e35-164230d1df67"),
            UUID.fromString("0bacfc4c-4da3-11e4-9e35-164230d1df67"),
            UUID.fromString("0bacfd6e-4da3-11e4-9e35-164230d1df67")};

    // Member fields
    private final BluetoothAdapter mAdapter;
    private final Handler mHandler;
    // private AcceptThread mAcceptThread;
    // public ArrayList<ConnectThread> mConnectThreads = new
    // ArrayList<ConnectThread>();
    // public ArrayList<ConnectedThread> mConnectedThreads = new
    // ArrayList<ConnectedThread>();
    ConnectThread currentConnectThread1;
    ConnectThread currentConnectThread2;
    ConnectedThread currentConnectedThread1;
    ConnectedThread currentConnectedThread2;
    // public ConnectThread mConnectThread;
    // public ConnectedThread mConnectedThread;
    private int mState;
    // Constants that indicate the current connection state
    public static final int STATE_NONE = 0; // we're doing nothing
    public static final int STATE_LISTEN = 1; // now listening for incoming
    // connections
    public static final int STATE_CONNECTING = 2; // now initiating an outgoing
    // connection
    public static final int STATE_CONNECTED_CLIENT = 3; // now connected to a
    // remote device
    public static final int STATE_CONNECTED_SERVER = 4;

    /**
     * Constructor. Prepares a new BluCloud session.
     *
     * @param context The UI Activity Context
     * @param handler A Handler to send messages back to the UI Activity
     */

    public Client(Context context, Handler handler, BluCloud parent) {
        mAdapter = BluetoothAdapter.getDefaultAdapter();
        mState = STATE_NONE;
        mHandler = handler;
    }

    /**
     * Set the current state of the chat connection
     *
     * @param state An integer defining the current connection state
     */
    private void setState(int state) {
        if (D)
            Log.d(TAG, "setState() " + mState + " -> " + state);
        mState = state;

        // Give the new state to the Handler so the UI Activity can update
        mHandler.obtainMessage(BluCloud.MESSAGE_STATE_CHANGE, state, -1)
                .sendToTarget();
    }

    /**
     * Return the current connection state.
     */
    public int getState() {
        return mState;
    }

    public synchronized void clearCurrentConnections() {
        // int length = mConnectThreads.size();
        // for (int i = 0; i < length; i++) {
        // ConnectThread tmp = mConnectThreads.get(i);
        // tmp.cancel();
        // tmp = null;
        // }
        // mConnectThreads.clear();
        // length = mConnectedThreads.size();
        // for (int i = 0; i < length; i++) {
        // ConnectedThread tmp = mConnectedThreads.get(i);
        // tmp.cancel();
        // tmp = null;
        // }
        // mConnectedThreads.clear();
        if (currentConnectThread1 != null) {
            currentConnectThread1.cancel();
            currentConnectThread1 = null;
        }
        if (currentConnectThread2 != null) {
            currentConnectThread2.cancel();
            currentConnectThread2 = null;
        }
        if (currentConnectedThread1 != null) {
            currentConnectedThread1.cancel();
            currentConnectedThread1 = null;
        }
        if (currentConnectedThread2 != null) {
            currentConnectedThread2.cancel();
            currentConnectedThread2 = null;
        }
    }

    /**
     * Start the chat service. Specifically start AcceptThread to begin a
     * session in listening (server) mode. Called by the Activity onResume()
     */
    public void start() {
        if (D)
            Log.d(TAG, "start");

        clearCurrentConnections();
        setState(STATE_LISTEN);

    }

    /**
     * Start the ConnectThread to initiate a connection to a remote device.
     *
     * @param mNewDevices The BluetoothDevice to connect
     */
    public synchronized void connect(ArrayList<BluetoothDevice> mNewDevices) {
        if (D)
            Log.d(TAG, "connect to: " + mNewDevices);
        // if (currentConnectThread == null
        // && ((listener.serverSideClient == null) ||
        // (!listener.serverSideClient
        // .getAddress().equals(mNewDevices.getAddress())))) {
        ArrayList<BluetoothDevice> NewDevices1 = new ArrayList<BluetoothDevice>(
                mNewDevices);
        ArrayList<BluetoothDevice> NewDevices2 = new ArrayList<BluetoothDevice>(
                mNewDevices);
        currentConnectThread1 = new ConnectThread(NewDevices1);
        currentConnectThread1.start();
        currentConnectThread2 = new ConnectThread(NewDevices2);
        currentConnectThread2.start();
        setState(STATE_CONNECTING);
        // } else {
        // if (D)
        // Log.d(TAG, "already connected to: " + mNewDevices);
        // }
    }

    public synchronized void connected(BluetoothSocket socket,
                                       BluetoothDevice device) {
        if (D)
            Log.d(TAG, "connected");
        if (currentConnectedThread1 == null) {
            currentConnectedThread1 = new ConnectedThread(socket);
            currentConnectedThread1.start();
        } else {
            currentConnectedThread2 = new ConnectedThread(socket);
            currentConnectedThread2.start();
        }

        Message msg = mHandler.obtainMessage(BluCloud.MESSAGE_DEVICE_NAME);
        Bundle bundle = new Bundle();
        bundle.putString(BluCloud.DEVICE_NAME, device.getName());
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        setState(STATE_CONNECTED_CLIENT);
    }

    /**
     * Stop all threads
     */
    public void stop() {
        if (D)
            Log.d(TAG, "stop");

        clearCurrentConnections();
        setState(STATE_NONE);
    }

	/*
     * public void sendMessage(String message_remote) { byte[] send =
	 * message_remote.getBytes(); write(send); }
	 */
    /**
     * Write to the ConnectedThread in an unsynchronized manner
     *
     * @param out
     *            The bytes to write
     * @see com.blucloud.kareemadelfathi.blucloud.Client.ConnectedThread#write(byte[])
     */
    /*
	 * public synchronized void write(byte[] out) { // Create temporary object
	 * ConnectedThread r; // Synchronize a copy of the ConnectedThread int
	 * length = mConnectedThreads.size(); for (int i = 0; i < length; i++) {
	 * synchronized (this) { // if (mState != STATE_CONNECTED_CLIENT) // return;
	 * r = mConnectedThreads.get(i); } // Perform the write unsynchronized
	 * r.write(out); }
	 * 
	 * }
	 */
    /**
     * Indicate that the connection attempt failed and notify the UI Activity.
     */
	/*
	 * private void connectionFailed() { setState(STATE_LISTEN);
	 * 
	 * // Send a failure message_remote back to the Activity Message msg =
	 * mHandler.obtainMessage(BluCloud.MESSAGE_TOAST); Bundle bundle = new
	 * Bundle(); bundle.putString(BluCloud.TOAST,
	 * "Unable to connect device"); msg.setData(bundle);
	 * mHandler.sendMessage(msg); // stop(); // parent.freeMemory(this); }
	 */

    /**
     * Indicate that the connection was lost and notify the UI Activity.
     */
	/*
	 * private void connectionLost() {
	 * 
	 * setState(STATE_LISTEN); // Send a failure message_remote back to the Activity
	 * Message msg = mHandler.obtainMessage(BluCloud.CONNECTION_LOST);
	 * Bundle bundle = new Bundle(); bundle.putString(BluCloud.TOAST,
	 * "Device connection was lost"); msg.setData(bundle);
	 * mHandler.sendMessage(msg); // stop(); // parent.freeMemory(this); }
	 */
    public String[] getRemoteBluetoothName() {
        return remoteBluetoothName;
    }

    public void setRemoteBluetoothName(String[] remoteBluetoothName) {
        this.remoteBluetoothName = remoteBluetoothName;
    }

    /**
     * This thread runs while attempting to make an outgoing connection with a
     * device. It runs straight through; the connection either succeeds or
     * fails.
     */
    // public int i = 0;

    private class ConnectThread extends Thread {
        private BluetoothSocket mmSocket;
        private BluetoothDevice mmDevice;
        ArrayList<BluetoothDevice> mNewDevices;

        public ConnectThread(ArrayList<BluetoothDevice> mNewDevices) {
            this.mNewDevices = mNewDevices;
            if (mNewDevices.size() > 0) {
                mmDevice = mNewDevices.remove(0);
            }

        }

        public void run() {
            Log.i(TAG, "BEGIN mConnectThread");
            setName("ConnectThread");

            // BluetoothSocket tmp = null;
            // Always cancel discovery because it will slow down a connection
            if (mmDevice == null)
                return;
            mAdapter.cancelDiscovery();
            if ((listener.serverSideClient == null)
                    || (!listener.serverSideClient.getAddress().equals(
                    mmDevice.getAddress()))) {

                // Make a connection to the BluetoothSocket
                try {
                    // This is a blocking call and will only return on a
                    // successful connection or an exception
                    mmSocket = mmDevice
                            .createInsecureRfcommSocketToServiceRecord(MY_UUID[0]);
                    mmSocket.connect();
                } catch (IOException e) {
                    e.printStackTrace();
                    currentConnectThread1 = new ConnectThread(mNewDevices);
                    currentConnectThread1.start();
                }

                if (mmSocket != null && mmSocket.isConnected()) {
                    connected(mmSocket, mmDevice);
                }
            } else {
                currentConnectThread1 = new ConnectThread(mNewDevices);
                currentConnectThread1.start();
            }
        }

        public void cancel() {
            try {
                if (mmSocket != null)
                    mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "close() of connect socket failed", e);
            }
        }
    }

    /**
     * This thread runs during a connection with a remote device. It handles all
     * incoming and outgoing transmissions.
     */

    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        // private final OutputStream mmOutStream;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        //Timer timerReciever;
        //TimerTask timerRecieverDescription;
        //Timer timerSender;
        //TimerTask timerSenderDescription;

        public ConnectedThread(BluetoothSocket socket) {
            Log.d(TAG, "create ConnectedThread");
            mmSocket = socket;
            InputStream tmpIn = null;
            // OutputStream tmpOut = null;

            // Get the BluetoothSocket input and output streams
            try {
                tmpIn = socket.getInputStream();
                // tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.e(TAG, "temp sockets not created", e);
            }

            mmInStream = tmpIn;
            // mmOutStream = tmpOut;
            /*
            timerRecieverDescription = new TimerTask() {
                @Override
                public void run() {
                    try {
                        if (mmInStream.available() > 0) {
                            mmInStream.read(tmpDataSize);
                            String tmp = "";
                            for (int i = 0; i < tmpDataSize.length; i++) {
                                if (tmpDataSize[i] == 0) {
                                    tmp += "0";
                                } else {
                                    tmp += "1";
                                }
                            }
                            tmpDataSize = new byte[32];
                            int dataLength = Integer.parseInt(tmp, 2);
                            for (int i = 0; i < dataLength; i++) {
                                baos.write(mmInStream.read());
                            }
                            if (baos.size() != 0) {
                                mHandler.obtainMessage(
                                        BluCloud.MESSAGE_READ,
                                        baos.size(), -1, baos.toByteArray())
                                        .sendToTarget();
                                try {
                                    baos.close();
                                } catch (IOException e1) {
                                    // TODO Auto-generated catch block
                                    e1.printStackTrace();
                                }
                                baos = new ByteArrayOutputStream();

                            }
                        }
                    } catch (IOException e) {
                        Log.e(TAG, "disconnected", e);

                    }
                }
            };
            timerReciever = new Timer("timerReciever");

            timerSenderDescription = new TimerTask() {
                @Override
                public void run() {
                    if (this != null && mmSocket != null
                            && mmSocket.isConnected()) {
                        write(BluCloud.DBAllData);
                    }
                }
            };
            timerSender = new Timer("timerSender");
            // handler.getLooper();
            // (new senderThread()).start();
*/
        }


        byte[] tmpDataSize = new byte[32];

        // recieverThread rt;

        public void run() {
            Log.i(TAG, "BEGIN mConnectedThread");
            //timerReciever
            //        .scheduleAtFixedRate(timerRecieverDescription, 0, 1500);
            //timerSender.scheduleAtFixedRate(timerSenderDescription, 0, 2000);
            while (mmSocket != null && mmSocket.isConnected()) {
                try {
                    if (mmInStream.available() > 0) {
                        mmInStream.read(tmpDataSize);
                        String tmp = "";
                        for (int i = 0; i < tmpDataSize.length; i++) {
                            if (tmpDataSize[i] == 0) {
                                tmp += "0";
                            } else {
                                tmp += "1";
                            }
                        }
                        tmpDataSize = new byte[32];
                        int dataLength = Integer.parseInt(tmp, 2);
                        for (int i = 0; i < dataLength; i++) {
                            baos.write(mmInStream.read());
                        }
                        if (baos.size() != 0) {
                            mHandler.obtainMessage(
                                    BluCloud.MESSAGE_READ,
                                    baos.size(), -1, baos.toByteArray())
                                    .sendToTarget();
                            try {
                                baos.close();
                            } catch (IOException e1) {
                                // TODO Auto-generated catch block
                                e1.printStackTrace();
                            }
                            baos = new ByteArrayOutputStream();

                        }
                    }
                } catch (IOException e) {
                    Log.e(TAG, "disconnected", e);

                }

                if (this != null && mmSocket != null
                        && mmSocket.isConnected()) {
                    write(BluCloud.DBAllData);
                }
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            // rt = new recieverThread();
            // rt.start();

        }

        public byte[] includeSizeToBuffer(byte[] buffer) throws IOException {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            String sizeInBinaryStringIncomplete = Integer
                    .toBinaryString(buffer.length);
            String sizeInBinaryString = "";
            for (int i = sizeInBinaryStringIncomplete.length(); i <= 31; i++) {
                sizeInBinaryString += "0";
            }
            sizeInBinaryString += sizeInBinaryStringIncomplete;
            byte[] tmp = new byte[sizeInBinaryString.length()];
            for (int i = 0; i <= 31; i++) {
                if (sizeInBinaryString.charAt(i) == '0') {
                    tmp[i] = 0;
                } else {
                    tmp[i] = 1;
                }
            }
            baos.write(tmp);
            baos.write(buffer);
            return baos.toByteArray();
        }

        /**
         * Write to the connected OutStream.
         *
         * @param buffer
         * The bytes to write
         */
        byte[] last_sent;

        public synchronized void write(byte[] buffer) {
            try {
                if (last_sent != buffer) {
                    mmSocket.getOutputStream().write(
                            includeSizeToBuffer(buffer));
                    last_sent = buffer;
                    BluCloud.tmp_SENT++;
                }
                // Share the sent message_remote back to the UI Activity
                // mHandler.obtainMessage(BluCloud.MESSAGE_WRITE, -1, -1,
                // buffer).sendToTarget();

            } catch (IOException e) {

                Log.e(TAG, "Exception during write , socket connection is : "
                        + mmSocket.isConnected(), e);

            }
        }

        public void cancel() {
            try {
                // rt.interrupt();
                //timerReciever.cancel();
                //timerSender.cancel();
                mmSocket.getOutputStream().close();
                mmSocket.getInputStream().close();
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "close() of connect socket failed", e);
            }
        }
    }
}
